import React from 'react'; 
import{Text,View} from "react-native";
import AppIntroSlider from 'react-native-app-intro-slider';
import { responsiveWidth, responsiveFontSize ,responsiveHeight} from 'react-native-responsive-dimensions';


export default class Walk extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  _onDone = () => {
   this.props.navigation.navigate("auth");
  };
  renderfun = () =>{
   var slides=[];
    return(
      slides = [
        {
          key: 's1',
          title:"Welcome To ShareWheelz",
          text: 'ShareWheelz is a carpooling service that helps you find, connect others going your way.',
          titleStyle: {color:"#0652dd",fontSize:responsiveFontSize(3.5),fontWeight:"bold"},
          textStyle: {color:"#000",fontSize:responsiveFontSize(2.5)},
          image: require('../../Components/Images/Logo/LogoFull.png'),
          imageStyle: {height:responsiveWidth(40),width:responsiveWidth(45)},
          backgroundColor:"#fff"
        },
        {
          key: 's2',
          title: 'Verified Drivers',
          text: 'Verified drivers, who might be your colleagues and neighbours, offer rides during commute.',
          titleStyle: {color:"#0652dd",fontSize:responsiveFontSize(3.5),fontWeight:"bold"},
          textStyle: {color:"#000",fontSize:responsiveFontSize(2.5)},
          image: require('../../Components/Images/Logo/LogoFull.png'),
          imageStyle: {height:responsiveHeight(30),width:responsiveWidth(50),borderRadius:responsiveWidth(5)},
          backgroundColor:"#fff"
        },
        
        {
          key: 's3',
          title: "Saving Money & Gasoline ",
          text: 'ShareWheelz promise to help you save money on gas and reduce traffic and help in heaaling the earth. ',
          titleStyle: {color:"#0652dd",fontSize:responsiveFontSize(3.5),fontWeight:"bold"},
          textStyle: {color:"#000",fontSize:responsiveFontSize(2.5)},
          image: require('../../Components/Images/Logo/LogoFull.png'),
          imageStyle: {height:responsiveHeight(30),width:responsiveWidth(70),borderRadius:responsiveWidth(5)},
          backgroundColor:"#fff"
        },
        {
          key: 's4',
          title: "Refer and earn points",
          text: 'Invite your friends to Sharewheelz and get 100 points both for each referal. ',
          titleStyle: {color:"#0652dd",fontSize:responsiveFontSize(3.5),fontWeight:"bold"},
          textStyle: {color:"#000",fontSize:responsiveFontSize(2.5)},
          image: require('../../Components/Images/Logo/LogoFull.png'),
          imageStyle: {height:responsiveHeight(25),width:responsiveWidth(60),borderRadius:responsiveWidth(5)},
          backgroundColor:"#fff"
        },
      
      ]
    )
  }
  _renderNextButton = () => {return(<View style={{display:"none"}}></View>)};
  _renderDoneButton = () => {
    return (
      <View style={{alignItems:"flex-end",justifyContent:"center"}}>
        <View style={{height:responsiveHeight(5),width:responsiveWidth(30),backgroundColor:"#0652dd",
                  alignItems:"center",justifyContent:"center",borderRadius:responsiveWidth(3)}}>
          <Text style={{color:"#fff",fontSize:responsiveFontSize(2.5)}}>Get Started</Text>
        </View>
      </View>
    );
  };
  render() {
      return (
          // <AppIntroSlider
          //   slides={this.renderfun()}
          //   onDone={this._onDone}
          //   showNextButton={false}
          //   showNextButton={false}
          //   bottomButton={true}
          //   renderDoneButton={this._renderDoneButton}
          //   renderNextButton={this._renderNextButton}
          //   activeDotStyle={{backgroundColor:"#0652dd"}}
          //   dotStyle={{borderColor:"#0652dd",borderWidth:1}}
          // />
            <AppIntroSlider
            slides={this.renderfun()}
            onDone={this._onDone}
            showNextButton={false}
            showNextButton={false}
            bottomButton={true}
            renderDoneButton={this._renderDoneButton}
            renderNextButton={this._renderNextButton}
            activeDotStyle={{backgroundColor:"#0652dd"}}
            dotStyle={{borderColor:"#0652dd",borderWidth:1}}
           />
      );
  }
}


