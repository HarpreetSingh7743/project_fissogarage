import React, { Component } from 'react';
import { View, Text, Image, SafeAreaView,TextInput, KeyboardAvoidingView, TouchableWithoutFeedback, Keyboard, TouchableOpacity,AsyncStorage } from 'react-native';
import Styles from '../../Components/Styles/Styles';
import { responsiveHeight, responsiveWidth, responsiveFontSize } from 'react-native-responsive-dimensions';
import { Toast,Spinner } from 'native-base';
import KeyboardSpacer from "react-native-keyboard-spacer";
//import EnterOTPModal from '../../Components/CustomModals/EnterOTPModal';

export default class SignUp extends Component {
  state = {
    UserName: '',
    UserEmail: '',
    UserPassword: '',
    loading:false,
    visibleEnterOTPModal:true
  }

  verifyDetails() {
    let { UserName, UserEmail, UserPassword } = this.state
    let mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    let passformat=  /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,20}$/;

    if (UserName == "" || UserEmail == "" || UserPassword == "") {
      Toast.show({
        text: "Please fill all the details",
        textStyle: { textAlign: "center", color: '#111' },
        duration: 2000,
        position: 'top',
        style: { borderRadius: 10, margin: responsiveWidth(10), backgroundColor: 'rgba(255,255,255,0.8)' }
      });
    }
    else {
      if(UserName.length>=3){
        if(UserEmail.match(mailformat)){
          if(UserPassword.match(passformat)){
            this.sendOtpMail();
          }
          else{
            Toast.show({
              text: "Password length must be more than 8 and a combination of Capital, Small letters and Numbers",
              textStyle: { textAlign: "center", color: '#111' },
              duration: 5000,
              position: 'top',
              style: { borderRadius: 10, margin: responsiveWidth(10), backgroundColor: 'rgba(255,255,255,0.8)' }
            });
          }
        }
        else{
          Toast.show({
            text: "Please fill email in correct Format (i.e. abc@example.com)",
            textStyle: { textAlign: "center", color: '#111' },
            duration: 2000,
            position: 'top',
            style: { borderRadius: 10, margin: responsiveWidth(10), backgroundColor: 'rgba(255,255,255,0.8)' }
          });
        }
      }
      else{
        Toast.show({
          text: "Please enter your full Name",
          textStyle: { textAlign: "center", color: '#111' },
          duration: 2000,
          position: 'top',
          style: { borderRadius: 10, margin: responsiveWidth(10), backgroundColor: 'rgba(255,255,255,0.8)' }
        });
      }
  
    }
  }
  sendOtpMail= async()=>{
    this.setState({loading:true})
    try{
      fetch("http://yoyorooms.in/fisso_gargage/appapi/signup.php", 
          {
              method: 'POST',
              headers: {
                  Accept: 'application/json',
                  'Content-Type': 'application/json',
              },
              body: JSON.stringify({
                        u_name : this.state.UserName,
                        u_email :this.state.UserEmail,
                        u_pass : this.state.UserPassword,
                      }),     
          })
          .then(response => response.json())
          .then(response2 => {
           //console.log(response2)
            
              if(response2.status==1){
                
                this.setState({loading:false})
                AsyncStorage.setItem('uid',response2.user_data.UserId);
                AsyncStorage.setItem('uStatus',response2.user_data.status);
                // Toast.show({
                //   text: response2.message,
                //   textStyle: { textAlign: "center", color: '#111' },
                //   duration: 2000,
                //   position: 'top',
                //   style: { borderRadius: 10, margin: responsiveWidth(10), backgroundColor: 'rgba(255,255,255,0.8)' }
                // });
                this.props.navigation.navigate("enterOtpmodal",{u_email:this.state.UserEmail,u_otp:response2.user_data.otp})
                // this.setState({userDetails:response2.user_Details,seconds:120})
                // this.countSeconds();
               
              }
              else{
                this.setState({loading:false})
                Toast.show({
                  text: response2.message,
                  textStyle: { textAlign: "center", color: '#111' },
                  duration: 2000,
                  position: 'top',
                  style: { borderRadius: 10, margin: responsiveWidth(10), backgroundColor: 'rgba(255,255,255,0.8)' }
                });
              }
          }).catch((error) =>{
                this.setState({loading:false})
                console.log(error)
              // result.status = false;
              // result.data = undefined;
              // reject(result)
          });
      
  }
  catch(error){
        this.setState({loading:false})
        console.log(error)
      // result.status = false;
      // result.message = error;
      // result.data = undefined;
      // resolve(result)
  }
}
render() {
    return (
      <SafeAreaView style={Styles.container}>
          <TouchableWithoutFeedback style={Styles.container} onPress={Keyboard.dismiss}>
            <View style={{ flex: 1 }}>
              <View style={{ alignItems: 'center', justifyContent: 'center',marginTop:responsiveHeight(15) }}>
                <Image source={require('../../Components/Images/Logo/LogoFull.png')} 
                      style={{ height: responsiveHeight(14), width: responsiveWidth(58) }} />
                <Text style={{ fontSize: responsiveFontSize(2.2), fontWeight: 'bold', color: '#fff',
                              marginTop: responsiveWidth(1), textAlign: 'center' }}>
                  Bring your garage online
                </Text>
              </View>
              <View style={{padding: responsiveWidth(5),marginTop:responsiveHeight(10),paddingVertical:responsiveHeight(10)}}>
                <>
                <TextInput style={Styles.authInput}
                  placeholder="Enter Your Full Name"
                  placeholderTextColor="#fff"
                  keyboardType="default"
                  returnKeyType="next"
                  autoCorrect={false}
                  onSubmitEditing={() => { this.refs.email.focus() }} 
                  onChangeText={(UserName)=>{this.setState({UserName:UserName})}}/>
                  
                <TextInput style={Styles.authInput}
                  placeholder="Enter Your Email"
                  placeholderTextColor="#fff"
                  keyboardType="email-address"
                  returnKeyType="next"
                  ref={'email'}
                  autoCorrect={false}
                  onSubmitEditing={() => { this.refs.pass.focus() }} 
                  onChangeText={(UserEmail)=>{this.setState({UserEmail:UserEmail})}}/>

                <TextInput style={Styles.authInput}
                  placeholder="Enter Password"
                  placeholderTextColor="#fff"
                  keyboardType="default"
                  returnKeyType="go"
                  autoCorrect={false}
                  secureTextEntry={true}
                  ref={'pass'}
                  onChangeText={(UserPassword)=>{this.setState({UserPassword:UserPassword})}}
                  onSubmitEditing={() => { this.verifyDetails() }} />

                <View style={{ marginTop: responsiveWidth(8), flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                  {!this.state.loading &&
                    <TouchableOpacity style={Styles.WhitesubmitButton}
                      onPress={() => { this.props.navigation.navigate("SignIn") }}>
                      <Text style={Styles.BoldWhiteText}>SignIn</Text>
                    </TouchableOpacity>
                  }
                  {!this.state.loading &&
                    <TouchableOpacity style={Styles.BluesubmitButton} onPress={() => { this.verifyDetails() }}>
                      <Text style={Styles.BoldBlueText}>SignUp</Text>
                    </TouchableOpacity>
                  }
                  {this.state.loading &&
                    <TouchableOpacity style={Styles.BluesubmitButton}>
                      <Spinner size="small" color="#229CE3"/>
                    </TouchableOpacity>
                  }
                </View>
                </>
                
                <KeyboardSpacer/>
              </View>
              
            </View>
            
          </TouchableWithoutFeedback>
      </SafeAreaView>
    );
  }
}

