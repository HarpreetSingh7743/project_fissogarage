import React, { Component } from 'react';
import { View, Text, Image,TextInput,KeyboardAvoidingView,TouchableWithoutFeedback, Keyboard, TouchableOpacity,AsyncStorage } from 'react-native';
import Styles from '../../Components/Styles/Styles';
import { responsiveHeight, responsiveWidth, responsiveFontSize } from 'react-native-responsive-dimensions';
import { Toast,Spinner } from 'native-base';
import Localstorage from "../../../async";
import EnterOTPModal from '../../Components/CustomModals/EnterOTPModal';
import { State, ScrollView } from 'react-native-gesture-handler';
import {SafeAreaView} from "react-native-safe-area-context"

export default class SignIn extends Component {
    constructor(props) {
        super(props)
        this.state = {
            UserEmail: '',
            UserPassword: '',
            otp:'',
            visibleEnterOTPModal: false
        }
        this.setDevId();
    }
    setDevId = () => {
        if (Platform.OS == "android") {
            let DevId = "android";
            Localstorage.storeItem("DevId", DevId);
        }
        else {
            let DevId = "ios";
            Localstorage.storeItem("DevId", DevId);
        }
    }
    verifyDetails() {
        let { UserEmail, UserPassword } = this.state
        let mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        let passformat = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,20}$/;
        if (UserEmail == "" || UserPassword == "") {
            Toast.show({
                text: "Please fill all the details",
                textStyle: { textAlign: "center", color: '#111' },
                duration: 2000,
                position: 'top',
                style: { borderRadius: 10, margin: responsiveWidth(10), backgroundColor: 'rgba(255,255,255,0.8)' }
            });
        }
        else {
            if (UserEmail.match(mailformat)) {
                if (UserPassword.match(passformat)) {
                    this.checkuser();
                }
                else {
                    Toast.show({
                        text: "Password length must be more than 8 and a combination of Capital, Small letters and Numbers",
                        textStyle: { textAlign: "center", color: '#111' },
                        duration: 2000,
                        position: 'top',
                        style: { borderRadius: 10, margin: responsiveWidth(10), backgroundColor: 'rgba(255,255,255,0.8)' }
                    });
                }
            }
            else {
                Toast.show({
                    text: "Please fill email in correct Format (i.e. abc@example.com)",
                    textStyle: { textAlign: "center", color: '#111' },
                    duration: 2000,
                    position: 'top',
                    style: { borderRadius: 10, margin: responsiveWidth(10), backgroundColor: 'rgba(255,255,255,0.8)' }
                });
            }
        }
    }
    checkuser = () => {
        try{
            this.setState({loading:true})
            fetch("http://yoyorooms.in/fisso_gargage/appapi/login.php", 
                {
                    method: 'POST',
                    headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({
                              u_email : this.state.UserEmail,
                              u_pass : this.state.UserPassword,
                            }),     
                })
                .then(response => response.json())
                .then(response2 => {
                //console.log(response2)
                  
                    if(response2.status==1){
                      
                      this.setState({loading:false})
                      AsyncStorage.setItem('uid',response2.user_data.UserId);
                      AsyncStorage.setItem('uStatus',response2.user_data.status);
                      Toast.show({
                        text: response2.message,
                        textStyle: { textAlign: "center", color: '#111' },
                        duration: 2000,
                        position: 'top',
                        style: { borderRadius: 10, margin: responsiveWidth(10), backgroundColor: 'rgba(255,255,255,0.8)' }
                      });
                      if(response2.user_data.status==1){
                        this.props.navigation.navigate("main")
                      }
                      else{
                        this.props.navigation.navigate("mainGarage")
                      }
                     
                    }
                    else{
                      this.setState({loading:false})
                      Toast.show({
                        text: response2.message,
                        textStyle: { textAlign: "center", color: '#111' },
                        duration: 2000,
                        position: 'top',
                        style: { borderRadius: 10, margin: responsiveWidth(10), backgroundColor: 'rgba(255,255,255,0.8)' }
                      });
                    }
                }).catch((error) =>{
                      this.setState({loading:false})
                      console.log(error)
                });
            
        }
        catch(error){
              this.setState({loading:false})
              console.log(error)
        }
    }
    render() {
        return (
            <SafeAreaView style={{flex:1,backgroundColor:"#229CE3"}}>
                <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
                    <View style={{flex:1}}>
                            <View style={{ alignItems: 'center', justifyContent: 'center',marginTop:responsiveHeight(15)}}>

                            {/* {this.state.visibleEnterOTPModal && <EnterOTPModal otp={this.state.otp} UserEmail={this.state.UserEmail} onExit={(val) => { this.setState({ visibleEnterOTPModal: val }) }} />} */}
                                <Image source={require('../../Components/Images/Logo/LogoFull.png')} 
                                    style={{ height: responsiveHeight(14), width: responsiveWidth(58) }} />
                                <Text style={{ fontSize: responsiveFontSize(2.2), fontWeight: 'bold', color: '#fff', 
                                            marginTop: responsiveWidth(1), textAlign: 'center' }}>
                                    Bring your garage online
                                </Text>
                            </View>
                            <KeyboardAvoidingView style={Styles.container}>
                            <View style={{ padding:responsiveWidth(5),marginTop:responsiveHeight(20)}}>
                                <TextInput style={Styles.authInput}
                                    placeholder="Enter Email"
                                    placeholderTextColor="#fff"
                                    keyboardType="email-address"
                                    returnKeyType="next"
                                    autoCorrect={false}
                                    onChangeText={(UserEmail) => { this.setState({ UserEmail: UserEmail }) }}
                                    onSubmitEditing={() => { this.refs.pass.focus() }} />

                                <TextInput style={Styles.authInput}
                                    placeholder="Enter Password"
                                    placeholderTextColor="#fff"
                                    keyboardType="default"
                                    returnKeyType="go"
                                    autoCorrect={false}
                                    secureTextEntry={true}
                                    ref={'pass'}
                                    onChangeText={(UserPassword) => { this.setState({ UserPassword: UserPassword }) }}
                                    onSubmitEditing={() => { this.verifyDetails() }} />
                                <View style={{ flexDirection: 'row' }}>
                                    <View style={{ flex: 0.3 }}></View>
                                    <View style={{ flex: 0.7, alignItems: 'flex-end' }}>
                                        <TouchableOpacity onPress={() => { this.props.navigation.navigate("Forgot") }}>
                                            <Text style={Styles.BoldWhiteText}>ForgotPassword ?</Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                                {/* <View style={{ marginTop: responsiveWidth(4), flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                                    <TouchableOpacity style={Styles.WhitesubmitButton} 
                                        onPress={() => { this.props.navigation.navigate("SignUp") }}>
                                        <Text style={Styles.BoldWhiteText}>SignUp</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity style={Styles.BluesubmitButton} onPress={() => { this.verifyDetails() }}>
                                        <Text style={Styles.BoldBlueText}>SignIn</Text>
                                    </TouchableOpacity>
                                </View> */}
                                <View style={{ marginTop: responsiveWidth(8), flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                                {!this.state.loading &&
                                    <TouchableOpacity style={Styles.WhitesubmitButton}
                                    onPress={() => { this.props.navigation.navigate("SignUp") }}>
                                    <Text style={Styles.BoldWhiteText}>SignUp</Text>
                                    </TouchableOpacity>
                                }
                                {!this.state.loading &&
                                    <TouchableOpacity style={Styles.BluesubmitButton} onPress={() => { this.verifyDetails() }}>
                                    <Text style={Styles.BoldBlueText}>SignIn</Text>
                                    </TouchableOpacity>
                                }
                                {this.state.loading &&
                                    <TouchableOpacity style={Styles.BluesubmitButton}>
                                    <Spinner size="small" color="#229CE3"/>
                                    </TouchableOpacity>
                                }
                                </View>
                            </View>
                            </KeyboardAvoidingView>
                    </View>

                </TouchableWithoutFeedback>
            </SafeAreaView>
        );
    }
}

