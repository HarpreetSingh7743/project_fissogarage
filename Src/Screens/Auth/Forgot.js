import React, { Component } from 'react';
import { View, Text, Image, SafeAreaView, TextInput, KeyboardAvoidingView, TouchableWithoutFeedback, Keyboard, TouchableOpacity } from 'react-native';
import Styles from '../../Components/Styles/Styles';
import { Toast,Spinner } from 'native-base'
import { responsiveHeight, responsiveWidth, responsiveFontSize } from 'react-native-responsive-dimensions';

export default class Forgot extends Component {
  state = {
    currentScreen: "Verify",
    email: "",
    GeneratedOTP: "",
    otp: '',
    password: '',
    Cpassword: '',
    buttonText: 'Get OTP',
    loader:false,
    loading:false
  }

  renderScreen = () => {
    if (this.state.currentScreen == "Verify") {
      return (
        <View>
          <TextInput style={Styles.authInput}
            placeholder="Enter Email"
            placeholderTextColor="#fff"
            keyboardType="email-address"
            returnKeyType="next"
            autoCorrect={false}
            onChangeText={(email) => { this.setState({ email: email }) }}
            onSubmitEditing={() => { this.checkMail() }} />
        </View>
      )
    }
    else if (this.state.currentScreen == "otp") {
      return (
        <View>
          <TextInput style={Styles.authInput}
            placeholder="Enter OTP"
            placeholderTextColor="#fff"
            keyboardType="number-pad"
            returnKeyType="next"
            autoCorrect={false}
            onSubmitEditing={() => { this.refs.pass.focus() }}
            onChangeText={(otp) => { this.setState({ otp: otp }) }} />

          <TextInput style={Styles.authInput}
            placeholder="Enter New Password"
            placeholderTextColor="#fff"
            keyboardType="default"
            returnKeyType="next"
            ref={'pass'}
            autoCorrect={false}
            secureTextEntry={true}
            onSubmitEditing={() => { this.refs.cpass.focus() }}
            onChangeText={(password) => { this.setState({ password: password }) }} />

          <TextInput style={Styles.authInput}
            placeholder="Confirm New Password"
            placeholderTextColor="#fff"
            keyboardType="default"
            returnKeyType="go"
            autoCorrect={false}
            secureTextEntry={true}
            ref={'cpass'}
            onSubmitEditing={() => { this.verifyDetails() }}
            onChangeText={(Cpassword) => { this.setState({ Cpassword: Cpassword }) }} />
        </View>
      )
    }
  }
  renderButton() {
    if (this.state.currentScreen == "Verify") {
      return (
        <View>
          {!this.state.loading &&
            <TouchableOpacity style={Styles.BluesubmitButton} onPress={() => { this.checkMail() }}>
            <Text style={Styles.BoldBlueText}>{this.state.buttonText}</Text>
            </TouchableOpacity>
          }
          {this.state.loading &&
              <TouchableOpacity style={Styles.BluesubmitButton}>
              <Spinner size="small" color="#229CE3"/>
              </TouchableOpacity>
          }
        </View>
      )
    }
    else if (this.state.currentScreen == "otp") {
      return (
        <View>
          {/* <TouchableOpacity style={Styles.BluesubmitButton} onPress={() => { this.verifyDetails() }}>
            <Text style={Styles.BoldBlueText}>{this.state.buttonText}</Text>
          </TouchableOpacity> */}
          {!this.state.loader &&
            <TouchableOpacity style={Styles.BluesubmitButton} onPress={() => { this.verifyDetails() }}>
              <Text style={Styles.BoldBlueText}>{this.state.buttonText}</Text>
            </TouchableOpacity>
          }
          {this.state.loader &&
              <TouchableOpacity style={Styles.BluesubmitButton}>
                <Spinner size="small" color="#229CE3"/>
              </TouchableOpacity>
          }
        </View>
      )
    }
  }
  checkMail = () => {
    var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    var email = this.state.email

    if (email.match(mailformat)) {

      //Enter Mail API code here
      this.setState({loading:true})
      try{
          fetch("http://yoyorooms.in/fisso_gargage/appapi/mailsend.php", 
              {
                  method: 'POST',
                  headers: {
                      Accept: 'application/json',
                      'Content-Type': 'application/json',
                  },
                  body: JSON.stringify({
                              u_email :this.state.email,
                          }),     
              })
              .then(response => response.json())
              .then(response2 => {
              //console.log(response2)
                
                  if(response2.otpMailSend==1){
                    this.setState({loading:false})
                    this.setState({ currentScreen: "otp", buttonText: 'Change Password', GeneratedOTP: response2.userData.otp })
                    Toast.show({
                      text: "An OTP has been sent on this Registered Email, Enter the OTP here and Reset your Password",
                      textStyle: { textAlign: "center", color: '#111' },
                      duration: 10000,
                      position: 'top',
                      style: { borderRadius: 20, margin: responsiveWidth(10), backgroundColor: 'rgba(255,255,255,0.8)' }
                    });
                    
                   
                  }
                  else{
                    this.setState({loading:false})
                    Toast.show({
                      text: response2.message,
                      textStyle: { textAlign: "center", color: '#111' },
                      duration: 2000,
                      position: 'top',
                      style: { borderRadius: 10, margin: responsiveWidth(10), backgroundColor: 'rgba(255,255,255,0.8)' }
                    });
                  }
              }).catch((error) =>{
                    this.setState({loading:false})
                    console.log(error)
              });
          
      }
      catch(error){
            this.setState({loading:false})
            console.log(error)
      }
     
    }
    else {
      Toast.show({
        text: "Enter Email in correct format (i.e. abc@example.com)",
        textStyle: { textAlign: "center", color: '#111' },
        duration: 2000,
        position: 'top',
        style: { borderRadius: 20, margin: responsiveWidth(10), backgroundColor: 'rgba(255,255,255,0.8)' }
      });
    }
  }
  verifyDetails() {
    let { otp, password, Cpassword, GeneratedOTP } = this.state
    var passw = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,20}$/;
    if (otp != '' || password != '' || Cpassword != '') {
      if (otp.length == 4) {
        if (password.match(passw)) {
          if (Cpassword == password) {
            this.setState({loader:true})
            try{
                fetch("http://yoyorooms.in/fisso_gargage/appapi/forgotpassword.php", 
                    {
                        method: 'POST',
                        headers: {
                            Accept: 'application/json',
                            'Content-Type': 'application/json',
                        },
                        body: JSON.stringify({
                                    u_email :this.state.email,
                                    otp : otp,
                                    u_pass: password
                                }),     
                    })
                    .then(response => response.json())
                    .then(response2 => {
                    //console.log(response2)
                      
                        if(response2==1){
                          this.setState({loader:false})
                          Toast.show({
                            text: "Password Changed Successfully! SignIn Now.",
                            textStyle: { textAlign: "center", color: '#111' },
                            duration: 10000,
                            position: 'top',
                            style: { borderRadius: 20, margin: responsiveWidth(10), backgroundColor: 'rgba(255,255,255,0.8)' }
                          });
                          this.props.navigation.navigate("SignIn")
                          
                        
                        }
                        else{
                          this.setState({loader:false})
                          Toast.show({
                            text: "You entered a wrong OTP.",
                            textStyle: { textAlign: "center", color: '#111' },
                            duration: 5000,
                            position: 'top',
                            style: { borderRadius: 10, margin: responsiveWidth(10), backgroundColor: 'rgba(255,255,255,0.8)' }
                          });
                        }
                    }).catch((error) =>{
                          this.setState({loader:false})
                          console.log(error)
                    });
                
            }
            catch(error){
                  this.setState({loader:false})
                  console.log(error)
            }
          }
          else {
            Toast.show({
              text: "Password didn't match",
              textStyle: { textAlign: "center", color: '#111' },
              duration: 2000,
              position: 'top',
              style: { borderRadius: 20, margin: responsiveWidth(10), backgroundColor: 'rgba(255,255,255,0.8)' }
            });
          }
        }
        else {
          Toast.show({
            text: "Password length must be more then 8 and a combination of Capital,Small leters and Numbers",
            textStyle: { textAlign: "center", color: '#111' },
            duration: 4000,
            position: 'top',
            style: { borderRadius: 20, margin: responsiveWidth(10), backgroundColor: 'rgba(255,255,255,0.8)' }
          });
        }
      }
      else {
        Toast.show({
          text: "Please fill the correct format OTP",
          textStyle: { textAlign: "center", color: '#111' },
          duration: 2000,
          position: 'top',
          style: { borderRadius: 20, margin: responsiveWidth(10), backgroundColor: 'rgba(255,255,255,0.8)' }
        });
      }
    }
    else {
      Toast.show({
        text: "Enter all Details",
        textStyle: { textAlign: "center", color: '#111' },
        duration: 2000,
        position: 'top',
        style: { borderRadius: 20, margin: responsiveWidth(10), backgroundColor: 'rgba(255,255,255,0.8)' }
      });
    }
  }

  render() {
    return (
      <SafeAreaView style={Styles.container}>
        <KeyboardAvoidingView style={Styles.container}>
          <TouchableWithoutFeedback style={Styles.container} onPress={Keyboard.dismiss}>
            <View style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }}>
              <View style={{ alignItems: 'center', justifyContent: 'center', flex: 1, marginBottom: responsiveWidth(13) }}>
                <Image source={require('../../Components/Images/Logo/LogoFull.png')} style={{ height: responsiveHeight(14), width: responsiveWidth(58) }} />
                <Text style={{ fontSize: responsiveFontSize(2.2), fontWeight: 'bold', color: '#fff', marginTop: responsiveWidth(1), textAlign: 'center' }}>Bring your garage online</Text>
              </View>
              <View style={{ position: "absolute", left: 0, right: 0, bottom: 0, height: responsiveHeight(30), padding: 20, }}>
                {this.renderScreen()}
                <View style={{ marginTop: responsiveWidth(4), flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                  {/* <TouchableOpacity style={Styles.WhitesubmitButton} onPress={() => { this.props.navigation.navigate("SignIn") }}>
                    <Text style={Styles.BoldWhiteText}>SignIn</Text>
                  </TouchableOpacity> */}
                  {!this.state.loading &&
                      <TouchableOpacity style={Styles.WhitesubmitButton}
                      onPress={() => { this.props.navigation.navigate("SignIn") }}>
                      <Text style={Styles.BoldWhiteText}>SignIn</Text>
                      </TouchableOpacity>
                  }
                  <View>
                    {this.renderButton()}
                  </View>
                </View>
              </View>
            </View>
          </TouchableWithoutFeedback>
        </KeyboardAvoidingView>
      </SafeAreaView>
    );
  }
}

