import React, { Component } from 'react';
import { View, Text, ImageBackground, Dimensions, SafeAreaView, TouchableOpacity, Modal, AsyncStorage, Image } from 'react-native';
import { Container, Content, Card, CardItem, Toast, Spinner } from 'native-base';
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions';
import ChangePasswordModal from '../../Components/CustomModals/ChangePasswordModal';
import GarageServicesModal from '../../Components/CustomModals/GarageServicesModal';
import GarageVehiclesModal from '../../Components/CustomModals/GarageVehiclesModal';
import DeleteAccountModal from '../../Components/CustomModals/DeleteAccountModal';
import Styles from '../../Components/Styles/Styles';
import Shimmer from '../../Components/CustomModals/shimmer';

var height = Dimensions.get("screen").height
var width = Dimensions.get("screen").width
class Profile extends Component {

    constructor(props) {
        super(props);
        this.state = {
            userData: [],
            two_wheelers: [],
            four_wheelers: [],
            two_Wheeler_services: [],
            four_Wheeler_services: [],
            visibleChangePasswordModal: false,
            visibleGarageServicesModal: false,
            visibleGarageVehiclesModal: false,
            visibleDeleteAccountModal: false,
            logoutLoader: false,
            loading: true,
            loader: true

        };
    }
    componentDidMount = () => {
        this.getDetails();
    }
    getDetails = async () => {
        const uid = await AsyncStorage.getItem('uid');
        // console.log(uid)
        try {
            fetch("https://yoyorooms.in/fisso_gargage/appapi/garagedetails.php",
                {
                    method: 'POST',
                    headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({
                        u_id: uid,
                    }),
                })
                .then(response => response.json())
                .then(response2 => {
                    //console.log(response2.garage_info.garage_details)

                    if (response2) {
                        this.setState({
                            userData: response2.user_info,
                            ownerInfo: response2.owner_info,
                            garageInfo: response2.garage_info.garage_details,
                            four_Wheeler_services: response2.garage_info.garage_Services.four_wheeler,
                            two_Wheeler_services: response2.garage_info.garage_Services.two_wheeler,
                            two_wheelers: response2.garage_info.garage_vechiles.two_wheeler,
                            four_wheelers: response2.garage_info.garage_vechiles.four_wheeler
                        })
                        //console.log(this.state.two_wheelers)
                        this.setState({ loading: false, loader: false })
                    }
                    else {
                        this.setState({ loading: false, loader: false })
                        Toast.show({
                            text: response2.message,
                            textStyle: { textAlign: "center", color: '#111' },
                            duration: 2000,
                            position: 'top',
                            style: { borderRadius: 10, margin: responsiveWidth(10), backgroundColor: 'rgba(255,255,255,0.8)' }
                        });
                    }
                }).catch((error) => {
                    this.setState({ loading: false, loader: false })
                    console.log(error)
                });

        }
        catch (error) {
            this.setState({ loading: false, loader: false })
            console.log(error)
        }
    }
    logOut = async () => {
        this.setState({ logoutLoader: true })
        await AsyncStorage.removeItem("uid");
        this.props.navigation.navigate("auth")
        this.setState({ logoutLoader: false })
    }
    render() {
        //console.log(this.state.garageInfo.GarageName);
        //this._renderLoader()
        return (
            <Container>
                {/* {!(this.state.loader) && */}
                <ImageBackground style={{ width: width, height: height }}
                    source={require('../../Components/Images/BG/Back.jpg')} resizeMode="cover">
                    <View style={{ marginTop: responsiveWidth(5), paddingHorizontal: responsiveWidth(5) }}>
                        {this.state.loader &&
                            <Shimmer width={responsiveWidth(60)} height={responsiveHeight(4.75)} color="#ccc" />
                        }
                        {!this.state.loader &&
                            <Text style={Styles.HeaderText}>
                                {this.state.garageInfo.GarageName}
                            </Text>
                        }
                        {this.state.loader &&
                            <View style={{ marginTop: responsiveHeight(1) }}>
                                <Shimmer width={responsiveWidth(50)} height={responsiveHeight(3)} color="#ccc" />
                            </View>
                        }
                        {!this.state.loader &&
                            <Text style={Styles.intermediateBoldText}>
                                {this.state.garageInfo.GarageEmail}
                            </Text>
                        }
                        {this.state.loader &&
                            <View style={{ marginTop: responsiveHeight(1) }}>
                                <Shimmer width={responsiveWidth(50)} height={responsiveHeight(3)} color="#ccc" />
                            </View>
                        }
                        {!this.state.loader &&
                            <Text style={{
                                color: '#fff',
                                fontSize: responsiveFontSize(2.5),
                                fontWeight: 'bold',
                                width: responsiveWidth(64)
                            }}>
                                {this.state.garageInfo.GarageAddress}
                            </Text>
                        }
                        {this.state.loader &&
                            <View style={{ marginTop: responsiveHeight(1) }}>
                                <Shimmer width={responsiveWidth(50)} height={responsiveHeight(3)} color="#ccc" />
                            </View>
                        }
                        {!this.state.loader &&
                            <Text style={Styles.BoldWhiteText}>
                                +91 {this.state.garageInfo.GaragePhone}
                            </Text>
                        }
                    </View>
                    <View style={{ marginTop: responsiveWidth(25), flex: 1 }}>
                        <Content>
                            <View style={{ marginBottom: responsiveHeight(20), paddingHorizontal: responsiveWidth(10) }} >
                                <View style={{ justifyContent: "center" }}>
                                    <View style={{ flexDirection: 'row', alignItems: "center" }}>
                                        <Text style={Styles.BoldBlueText}>Verified Garage : </Text>
                                        {(this.state.loader) &&
                                            <View>
                                                <Shimmer width={responsiveWidth(30)} height={responsiveHeight(3)} color="#ccc" />
                                            </View>
                                        }
                                        {!(this.state.loader) &&
                                            <View style={{ flexDirection: 'row', alignItems: "center", }}>
                                                <TouchableOpacity
                                                    onPress={() => {
                                                        this.state.garageInfo.verifiedGarage == 0 ?
                                                            alert("Complete 15 requests by diffrent customers to get verified on FISSO") :
                                                            alert("Verified! Completed 15 requests by diffrent customers on FISSO")
                                                    }}>
                                                    <Text style={{ color: this.state.garageInfo.verifiedGarage == 0 ? '#f11' : "#008000", fontWeight: 'bold' }}>
                                                        {this.state.garageInfo.verifiedGarage == 0 ? "Not Verified" : "Verified"}
                                                    </Text>
                                                </TouchableOpacity>
                                            </View>
                                        }
                                    </View>
                                    <View style={{ flexDirection: 'row', marginTop: 20, alignItems: "center" }}>
                                        <Text style={Styles.BoldBlueText}>Owner Name :  </Text>
                                        {this.state.loader &&
                                            <View>
                                                <Shimmer width={responsiveWidth(30)} height={responsiveHeight(3)} color="#ccc" />
                                            </View>
                                        }
                                        {!this.state.loader &&
                                            <Text style={Styles.boldBlackText}>
                                                {this.state.ownerInfo.OwnerName}
                                            </Text>
                                        }
                                    </View>
                                    <View style={{ flexDirection: 'row', marginTop: 20 }}>
                                        <Text style={Styles.BoldBlueText}>Owner Email :  </Text>
                                        {this.state.loader &&
                                            <View>
                                                <Shimmer width={responsiveWidth(30)} height={responsiveHeight(3)} color="#ccc" />
                                            </View>
                                        }
                                        {!this.state.loader &&
                                            <Text style={Styles.boldBlackText}>
                                                {this.state.ownerInfo.OwnerEmail}
                                            </Text>
                                        }
                                    </View>
                                    <View style={{ flexDirection: 'row', marginTop: 20 }}>
                                        <Text style={Styles.BoldBlueText}>Owner Contact :  </Text>
                                        {this.state.loader &&
                                            <View>
                                                <Shimmer width={responsiveWidth(30)} height={responsiveHeight(3)} color="#ccc" />
                                            </View>
                                        }
                                        {!this.state.loader &&
                                            <Text style={Styles.boldBlackText}>
                                                +91 {this.state.ownerInfo.OwnerMobile}
                                            </Text>
                                        }
                                    </View>
                                    <View style={{ flexDirection: 'row', marginTop: 20, alignItems: "center" }}>
                                        <Text style={Styles.BoldBlueText}>Open Days :  </Text>
                                        {this.state.loader &&
                                            <View>
                                                <Shimmer width={responsiveWidth(30)} height={responsiveHeight(3)} color="#ccc" />
                                            </View>
                                        }
                                        {!this.state.loader &&
                                            <Text style={Styles.boldBlackText}>
                                                {this.state.garageInfo.GarageOpenDays}
                                            </Text>
                                        }
                                    </View>
                                    <View style={{ flexDirection: 'row', marginTop: 20, alignItems: "center" }}>
                                        <Text style={Styles.BoldBlueText}>Timings :  </Text>
                                        {this.state.loader &&
                                            <View>
                                                <Shimmer width={responsiveWidth(30)} height={responsiveHeight(3)} color="#ccc" />
                                            </View>
                                        }
                                        {!this.state.loader &&
                                            <Text style={Styles.boldBlackText}>{this.state.garageInfo.GarageOpenTime} - {this.state.garageInfo.GarageCloseTime}
                                            </Text>
                                        }
                                    </View>
                                    <View style={{ flexDirection: 'row', marginTop: 20, alignItems: "center" }}>
                                        <Text style={Styles.BoldBlueText}>Zip Code :  </Text>
                                        {this.state.loader &&
                                            <View>
                                                <Shimmer width={responsiveWidth(30)} height={responsiveHeight(3)} color="#ccc" />
                                            </View>
                                        }
                                        {!this.state.loader &&
                                            <Text style={Styles.boldBlackText}>{this.state.garageInfo.GarageZIPcode}</Text>
                                        }
                                    </View>
                                    <View style={{ flexDirection: 'row', marginTop: 20 }}>
                                        <Text style={Styles.BoldBlueText}>Location :  </Text>
                                        {this.state.loader &&
                                            <View>
                                                <Shimmer width={responsiveWidth(30)} height={responsiveHeight(3)} color="#ccc" />
                                            </View>
                                        }
                                        {!this.state.loader &&
                                            <Text style={{ color: '#111', fontWeight: 'bold', width: responsiveWidth(50) }}>
                                                {this.state.garageInfo.GarageLocationID}</Text>
                                        }
                                    </View>
                                    <View style={{ flexDirection: 'row', marginTop: 20, alignItems: "center" }}>
                                        <Text style={Styles.BoldBlueText}>Password :  </Text>
                                        <Text style={{ color: '#111', fontWeight: 'bold', }}>********</Text>
                                        <TouchableOpacity onPress={() => { this.setState({ visibleChangePasswordModal: true }) }}>
                                            <Text style={{ color: '#33F', fontWeight: 'bold', marginLeft: responsiveWidth(17) }}>Change</Text>
                                        </TouchableOpacity>
                                    </View>
                                    <View style={{ flexDirection: 'row', marginTop: 20 }}>
                                        <Text style={Styles.BoldBlueText}>Garage Services :  </Text>
                                        <TouchableOpacity onPress={() => { this.setState({ visibleGarageServicesModal: true }) }}>
                                            <Text style={{ color: '#33f', fontWeight: 'bold', marginLeft: responsiveWidth(8) }}> View Services</Text>
                                        </TouchableOpacity>
                                    </View>
                                    <View style={{ flexDirection: 'row', marginTop: 20 }}>
                                        <Text style={Styles.BoldBlueText}>Vehicles :  </Text>
                                        <TouchableOpacity onPress={() => { this.setState({ visibleGarageVehiclesModal: true }) }}>
                                            <Text style={{ color: '#33f', fontWeight: 'bold', marginLeft: responsiveWidth(22) }}> View Vehicles</Text>
                                        </TouchableOpacity>
                                    </View>
                                    {/* GarageServicesModal is here */}

                                    {this.state.visibleGarageServicesModal && <GarageServicesModal
                                        two_Wheeler_services={this.state.two_Wheeler_services}
                                        four_Wheeler_services={this.state.four_Wheeler_services}
                                        onExit={(val) => { this.setState({ visibleGarageServicesModal: val }) }} />}
                                    {/* ChangePasswordModal is here*/}

                                    {this.state.visibleChangePasswordModal && <ChangePasswordModal onAction={(val) => this.setState({ visibleChangePasswordModal: val })}
                                        onConfirm={(visible) => {
                                            Toast.show({
                                                text: "Password Changed",
                                                textStyle: { textAlign: "center", color: '#fff' },
                                                duration: 2000,
                                                position: 'bottom',
                                                style: { borderRadius: 10, margin: responsiveWidth(10), backgroundColor: 'rgba(0,195,0,0.8)' }
                                            }),
                                                this.setState({ visibleChangePasswordModal: visible })
                                        }} />}

                                    {/* GarageVehiclesModal is here */}

                                    {this.state.visibleGarageVehiclesModal && <GarageVehiclesModal
                                        onExit={(val) => { this.setState({ visibleGarageVehiclesModal: val }) }} 
                                        two_wheelers={this.state.two_wheelers}
                                        four_wheelers={this.state.four_wheelers}/>}

                                    {/* DeleteAccountModal if here */}

                                    {this.state.visibleDeleteAccountModal && <DeleteAccountModal onExit={(val) => { this.setState({ visibleDeleteAccountModal: val }) }} />}
                                </View>
                                <View style={{ alignItems: 'center', justifyContent: 'center', marginTop: responsiveWidth(10), marginBottom: responsiveHeight(2), flexDirection: 'row' }}>
                                    {!this.state.logoutLoader &&
                                        <TouchableOpacity style={{
                                            width: responsiveWidth(30), alignItems: 'center', justifyContent: 'center', paddingHorizontal: responsiveWidth(3),
                                            height: responsiveHeight(6), borderColor: '#229CE3', borderWidth: 3,
                                            borderRadius: 10, marginHorizontal: responsiveWidth(2)
                                        }}
                                            onPress={() => this.logOut()}
                                        >
                                            <Text style={Styles.boldBlackText}>Logout</Text>
                                        </TouchableOpacity>
                                    }
                                    {this.state.logoutLoader &&
                                        <TouchableOpacity style={{
                                            width: responsiveWidth(30), alignItems: 'center', justifyContent: 'center', paddingHorizontal: responsiveWidth(3),
                                            height: responsiveHeight(6), borderColor: '#229CE3', borderWidth: 3,
                                            borderRadius: 10, marginHorizontal: responsiveWidth(2)
                                        }}
                                        >
                                            <Spinner size="small" color="#229CE3" />
                                        </TouchableOpacity>
                                    }

                                    <TouchableOpacity style={{
                                        width: "auto", alignItems: 'center', justifyContent: 'center', paddingHorizontal: responsiveWidth(3),
                                        height: responsiveHeight(6), borderColor: '#229CE3', borderWidth: 3,
                                        borderRadius: 10, marginHorizontal: responsiveWidth(2)
                                    }}
                                        onPress={() => { this.setState({ visibleDeleteAccountModal: true }) }}>
                                        <Text style={{ color: '#f11', fontWeight: 'bold' }}>Delete Account ?</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </Content>
                    </View>
                </ImageBackground>
                {/* } */}

            </Container >
        );
    }
}

export default Profile;
