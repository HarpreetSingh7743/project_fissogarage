import React, { Component } from 'react';
import { View, Text, TouchableOpacity, SafeAreaView, Platform, ScrollView, Modal, Alert } from 'react-native';
import { Container, Icon, Card, CardItem } from 'native-base';
import Styles from '../../Components/Styles/Styles';
import { responsiveWidth, responsiveHeight } from 'react-native-responsive-dimensions';
import EditServicesModal from '../../Components/CustomModals/EditServicesModal'

export default class TodayAppointments extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showdate: true,
      visibleModal: false,
      Temp: [],
      TotalBill:0,
      details: [
        {
          key: 1,
          Vehicle: "Maruti Swift",
          Year: "2018",
          RegNum: "CH01BJ9876",
          Status: "Completed",
          OwnerName: "Harpreet Singh",
          OwnerMob: "9876543210",
          AppointmentDate: "4/8/2020",
          ApponintmentTime: "10:30 AM",
          RequestedServices: [{ ServiceName: "service 1", Price: 300 }, { ServiceName: "Service 2", Price: 400 }, { ServiceName: "Service 3", Price: 100 }, { ServiceName: "service 4", Price: 1000 }]
        },
        {
          key: 2,
          Vehicle: "Honda City",
          Year: "2016",
          RegNum: "CH12BN1234",
          Status: "Completed",
          OwnerName: "Santosh Kumar",
          OwnerMob: "9988776543",
          AppointmentDate: "4/8/2020",
          ApponintmentTime: "11:30 AM",
          RequestedServices: [{ ServiceName: "service 1", Price: 100 }, { ServiceName: "Service 3", Price: 800 }, { ServiceName: "Service 5", Price: 200 }, { ServiceName: "service 8", Price: 1700 }]
        },
        {
          key: 3,
          Vehicle: "Honda Amaze",
          Year: "2019",
          RegNum: "CH12AN7777",
          Status: "Completed",
          OwnerName: "Ankit Kumar Saini",
          OwnerMob: "7896785642",
          AppointmentDate: "4/8/2020",
          ApponintmentTime: "12:30 AM",
          RequestedServices: [{ ServiceName: "service 4", Price: 390 }, { ServiceName: "Service 5", Price: 450 }, { ServiceName: "Service 7", Price: 300 }, { ServiceName: "service 8", Price: 100 }]
        }]
    };
  }

  SendDetails(){
    
    let TempArr = this.state.Temp
    let Temp1 = [], total = 0
    for (let i = 0; i < TempArr.length; i++) {
        Temp1.push(TempArr[i].Price)
    }
    for (let i = 0; i < Temp1.length; i++) {
        total = total + Temp1[i]
    }
    this.setState({ TotalBill: total,visibleModal:true })
    console.log(this.state.Temp);
    
  }
  renderItems() {
    return this.state.details.map((item, key) => {
      return (<Card key={key} style={{ paddingBottom:responsiveWidth(2), width: responsiveWidth(95), marginTop: responsiveWidth(3), borderRadius: 10 }}>
        <CardItem style={{ backgroundColor: '#229CE3', borderTopLeftRadius: 10, borderTopRightRadius: 10 }}>
          <Text style={Styles.BoldWhiteText}>{item.Vehicle} {item.Year}</Text>
        </CardItem>
        <View style={{ flex: 0.6, padding: responsiveWidth(2) }}>

          {this.state.visibleModal && <EditServicesModal TotalBill={this.state.TotalBill} inner={this.state.Temp} onAction={(val) => { this.setState({ visibleModal: val }) }} />}

          <View style={{ flexDirection: 'row' }}>
            <Text style={Styles.BoldBlueText}>Owner Name : </Text>
            <Text style={Styles.boldBlackText}>{item.OwnerName}</Text>
          </View>
          <View style={{ flexDirection: 'row' }}>
            <Text style={Styles.BoldBlueText}>Mobile : </Text>
            <Text style={Styles.boldBlackText}>{item.OwnerMob}</Text>
          </View>
          <View style={{ flexDirection: 'row' }}>
            <Text style={Styles.BoldBlueText}>Requested Services : </Text>
            <TouchableOpacity onPressIn={()=>{this.setState({ Temp: item.RequestedServices })}} onPressOut={() => {this.SendDetails() }}>
              <Text style={{ color: '#33f', fontWeight: 'bold' }}>View Services</Text>
            </TouchableOpacity>
          </View>
          <View style={{ flexDirection: 'row' }}>
            <Text style={Styles.BoldBlueText}>Apponintment Time : </Text>
            <Text>{item.ApponintmentTime}</Text>
          </View>
        </View>
        <View style={{ flex: 0.4, flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
          <TouchableOpacity style={Styles.BlueBorderButton} onPress={() => { this.DeleteAppointment(item, item.OwnerName) }}>
            <Text style={Styles.BoldBlueText}>Cancel</Text>
          </TouchableOpacity>
          <TouchableOpacity style={Styles.normalButton}>
            <Text style={Styles.BoldWhiteText}>Start</Text>
          </TouchableOpacity>
        </View>
      </Card>)
    })
  }

  DeleteAppointment = (key, name) => {
    var arr = this.state.details
    var pos = arr.indexOf(key);
    Alert.alert(
      "Cancel Appointment ?",
      "Are you sure you want to cancel " + name + "'s" + " Appointment?",
      [
        {
          text: "Cancel",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        },
        {
          text: "YES", onPress: () => {
            arr.splice(pos, 1)
            this.setState({ details: arr })
          }
        }
      ],
      { cancelable: true }
    );
  }

  showDate() {
    var Day = new Date().getDate()
    var Month = new Date().getMonth() + 1
    var Year = new Date().getFullYear()
    if (this.state.showdate == true) {
      return (
        <View>
          <Text style={Styles.boldBlackText}>{Day}/{Month}/{Year}</Text>
        </View>
      )
    }
  }
  render() {
    return (
      <Container>
        <View style={{ flex: 1, backgroundColor: '#F0F0F1' }}>

          <View style={{ flex: 0.1, backgroundColor: '#229CE3', justifyContent: (Platform.OS === "android") ? "center" : 'flex-start' }}>
            <View style={{ flexDirection: 'row', marginHorizontal: responsiveWidth(2), marginTop: responsiveWidth(6), alignItems: 'center' }}>
              <View style={{ flex: 0.6, flexDirection: 'row', alignItems: 'center', }}>
                <TouchableOpacity style={{ marginHorizontal: responsiveWidth(2), height: responsiveHeight(4), width: responsiveWidth(8) }} onPress={() => { this.props.navigation.goBack() }}>
                  <Icon name="md-arrow-back" type="Ionicons" style={{ color: '#fff' }} />
                </TouchableOpacity>
                <Text style={Styles.BoldWhiteText}>Today's Appointments</Text>
              </View>
              <View style={{ flex: 0.4, alignItems: 'flex-end' }}>
                {this.showDate()}
              </View>
            </View>
          </View>
          <View style={{ flex: 0.9, alignItems: 'center' }}>
            <ScrollView showsVerticalScrollIndicator={false}>
              {this.renderItems()}
            </ScrollView>
          </View>
        </View>
      </Container>
    );
  }
}

