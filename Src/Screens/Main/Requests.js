import React, { Component } from 'react';
import { View, Text, ImageBackground, Dimensions, TouchableOpacity, ScrollView, Alert } from 'react-native';
import { Container, Content, Card, CardItem } from 'native-base';
import { responsiveWidth, responsiveHeight } from 'react-native-responsive-dimensions';
import { SafeAreaView } from 'react-native-safe-area-context';
import call from 'react-native-phone-call';

import Styles from '../../Components/Styles/Styles';
import EditRequestModal from '../../Components/CustomModals/EditRequestModal'

var height = Dimensions.get("screen").height
var width = Dimensions.get("screen").width

class Requests extends Component {
  constructor(props) {
    super(props);
    this.state = {
      visibleModal: false,
      Temp: [],
      Requests: [
        {
          key: 1,
          Vehicle: "Maruti Swift",
          Year: "2018",
          RegNum: "CH01BJ9876",
          OwnerName: "Harpreet Singh",
          OwnerMob: "9876543210",
          RequestedServices: [{ ServiceName: "service 1", Price: 0 }, { ServiceName: "Service 2", Price: 0 }, { ServiceName: "Service 3", Price: 0 }, { ServiceName: "service 4", Price: 0 }]
        },
        {
          key: 2,
          Vehicle: "Honda City",
          Year: "2016",
          RegNum: "CH12BN1234",
          OwnerName: "Santosh Kumar",
          OwnerMob: "9988776543",
          RequestedServices: [{ ServiceName: "service 1", Price: 0 }, { ServiceName: "Service 3", Price: 0 }, { ServiceName: "Service 5", Price: 0 }, { ServiceName: "service 8", Price: 0 }, { ServiceName: "service 9", Price: 0 }]
        },
        {
          key: 3,
          Vehicle: "Honda Amaze",
          Year: "2019",
          RegNum: "CH12AN7777",
          OwnerName: "Ankit Kumar Saini",
          OwnerMob: "7896785642",
          RequestedServices: [{ ServiceName: "service 4", Price: 0 }, { ServiceName: "Service 5", Price: 0 }, { ServiceName: "Service 7", Price: 0 }, { ServiceName: "service 8", Price: 0 }]
        },
      ]
    };
  }
  //handler to make a call
  makecall = (t) => {
    const args = {
      number: t.OwnerMob,
      prompt: false,
    };
    let OwnerName = t.OwnerName
    let OwnerMob = t.OwnerMob
    Alert.alert(
      "Call " + OwnerName,
      "Call " + OwnerMob + " by pressing the 'Call' Button",
      [{ text: "Cancel", style: "cancel" },
      { text: "Call", onPress: () => call(args).catch(console.error) }],
      { cancelable: true }
    );
  };

  renderItem() {
    return this.state.Requests.map((item, key) => {
      return (<Card key={key} style={{ paddingBottom:responsiveWidth(2), width: responsiveWidth(95), marginTop: responsiveWidth(3), borderRadius: 10 }}>
        <CardItem style={{ backgroundColor: '#229CE3', borderTopLeftRadius: 10, borderTopRightRadius: 10 }}>
          <Text style={Styles.BoldWhiteText}>{item.Vehicle} {item.Year}  ( {item.RegNum} )</Text></CardItem>
        <View style={{ flex: 0.6, padding: responsiveWidth(2) }}>

          {this.state.visibleModal && <EditRequestModal inner={this.state.Temp} onAction={(val) => { this.setState({ visibleModal: val }) }} />}

          <View style={{ flexDirection: 'row' }}>
            <Text style={Styles.BoldBlueText}>Owner Name : </Text>
            <Text style={Styles.boldBlackText}>{item.OwnerName}</Text>
          </View>

          <View style={{ flexDirection: 'row' }}>
            <Text style={Styles.BoldBlueText}>Mobile : </Text>
            <Text style={Styles.boldBlackText}>{item.OwnerMob}</Text>
          </View>

          <View style={{ flexDirection: 'row' }}>
            <Text style={Styles.BoldBlueText}>Requested Services : </Text>
            <TouchableOpacity onPress={() => { this.setState({ Temp: item.RequestedServices, visibleModal: true }) }}>
              <Text style={{ color: '#33f', fontWeight: 'bold' }}>View Services</Text>
            </TouchableOpacity>
          </View>

        </View>
        <View style={{ flex: 0.4, flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
          <TouchableOpacity style={Styles.BlueBorderButton} onPress={()=>{this.makecall(item)}}>
            <Text style={Styles.BoldBlueText}>Contact Customer</Text>
          </TouchableOpacity>
          <TouchableOpacity style={Styles.normalButton}>
            <Text style={Styles.BoldWhiteText}>Accept</Text>
          </TouchableOpacity>

        </View>
      </Card>)
    })
  }
  render() {
    return (
        <ImageBackground source={require('../../Components/Images/BG/Back.jpg')}
          style={{ height: height, width: width, }}>
          <View style={{ flex: 1 }}>
            <View style={{ flex: 0.05, paddingLeft: responsiveWidth(2) }}>
              <Text style={Styles.intermediateBoldText}>Pending Requests</Text>
            </View>
            <View style={{ flex: 0.95, alignItems: 'center', }}>
              <ScrollView>
                {this.renderItem()}
              </ScrollView>
            </View>
          </View>
        </ImageBackground>
    );
  }
}

export default Requests;
