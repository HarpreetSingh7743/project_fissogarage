import React, { Component } from 'react';
import { View, Text, TouchableOpacity, SafeAreaView, Platform, ScrollView, Modal,Alert } from 'react-native';
import { Container, Icon, Right, Card, CardItem } from 'native-base';
import Styles from '../../Components/Styles/Styles';
import { responsiveWidth, responsiveHeight } from 'react-native-responsive-dimensions';
import DateTimePicker from 'react-native-modal-datetime-picker';
import moment, { months } from 'moment';
import call from 'react-native-phone-call';
import ShowServicesModal from '../../Components/CustomModals/ShowServicesModal';
export default class History extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showdate: true,
      VisibleDateTimePickerModal: false,
      visibleModal: false,
      selectedday: '',
      chosenDate: 'Select Appointment Date',
      details: [
        {
          key: 1, Vehicle: "Maruti Swift", Year: "2018", RegNum: "CH01BJ9876", Status: "Completed", OwnerName: "Harpreet Singh", OwnerMob: "9876543210", AppointmentDate: "4/8/2020", ApponintmentTime: "10:30 AM",
          RequestedServices: [{ ServiceName: "service 1", Price: 300 }, { ServiceName: "Service 2", Price: 400 }, { ServiceName: "Service 3", Price: 100 }, { ServiceName: "service 4", Price: 1000 }]
        },
        {
          key: 2, Vehicle: "Honda City", Year: "2016", RegNum: "CH12BN1234", Status: "Completed", OwnerName: "Santosh Kumar", OwnerMob: "9988776543", AppointmentDate: "4/8/2020", ApponintmentTime: "11:30 AM",
          RequestedServices: [{ ServiceName: "service 1", Price: 100 }, { ServiceName: "Service 3", Price: 800 }, { ServiceName: "Service 5", Price: 200 }, { ServiceName: "service 8", Price: 1700 }]
        },
        {
          key: 3, Vehicle: "Honda Amaze", Year: "2019", RegNum: "CH12AN7777", Status: "Completed", OwnerName: "Ankit Kumar Saini", OwnerMob: "7896785642", AppointmentDate: "4/8/2020", ApponintmentTime: "12:30 AM",
          RequestedServices: [{ ServiceName: "service 4", Price: 390 }, { ServiceName: "Service 5", Price: 450 }, { ServiceName: "Service 7", Price: 300 }, { ServiceName: "service 8", Price: 100 }]
        }],
      Temp: []
    };
  }

  renderItems() {
    return this.state.details.map((item, key) => {
      return (<Card key={key} style={{paddingBottom:responsiveWidth(2), width: responsiveWidth(95), marginTop: responsiveWidth(3), borderRadius: 10 }}>
        <CardItem style={{ backgroundColor: '#229CE3', borderTopLeftRadius: 10, borderTopRightRadius: 10 }}>
          <Text style={Styles.BoldWhiteText}>{item.Vehicle} {item.Year}  ( {item.RegNum} )</Text></CardItem>
        <View style={{ flex: 0.6, padding: responsiveWidth(2) }}>

          {this.state.visibleModal && <ShowServicesModal SelectedServices={this.state.Temp} onAction={(val) => { this.setState({ visibleModal: val }) }} />}

          <View style={{ flexDirection: 'row' }}>
            <Text style={Styles.BoldBlueText}>Owner Name : </Text>
            <Text style={Styles.boldBlackText}>{item.OwnerName}</Text>
          </View>
          <View style={{ flexDirection: 'row' }}>
            <Text style={Styles.BoldBlueText}>Mobile : </Text>
            <Text style={Styles.boldBlackText}>{item.OwnerMob}</Text>
          </View>
          <View style={{ flexDirection: 'row' }}>
            <Text style={Styles.BoldBlueText}>Requested Services : </Text>
            <TouchableOpacity onPress={() => { this.setState({ Temp: item.RequestedServices, visibleModal: true }) }}>
              <Text style={{ color: '#33f', fontWeight: 'bold' }}>View Services</Text>
            </TouchableOpacity>
          </View>
          <View style={{ flexDirection: 'row' }}>
            <Text style={Styles.BoldBlueText}>Apponintment Time : </Text>
            <Text>{item.ApponintmentTime}</Text>
          </View>
          <View style={{ flexDirection: 'row' }}>
            <Text style={Styles.BoldBlueText}>Status : </Text>
            <Text style={Styles.boldBlackText}>{item.Status}</Text>
          </View>
        </View>
        <View style={{ flex: 0.4, flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
          <View style={{ flex: 0.5 }}></View>
          <View style={{ flex: 0.5 }}>
            <TouchableOpacity style={Styles.normalButton} onPress={()=>{this.makecall(item)}}>
              <Text style={Styles.BoldWhiteText}>Contact Customer</Text>
            </TouchableOpacity>
          </View>

        </View>
      </Card>)
    })
  }

  showDate() {
    var Day = new Date().getDate()
    var Month = new Date().getMonth() + 1
    var Year = new Date().getFullYear()
    if (this.state.showdate == true) {
      return (
        <View>
          <Text style={Styles.boldBlackText}>{Day}/{Month}/{Year}</Text>
        </View>
      )
    }
  }
  //handler to make a call
  makecall = (t) => {
    const args = {
      number: t.OwnerMob,
      prompt: false,
    };
    let OwnerName = t.OwnerName
    let OwnerMob = t.OwnerMob
    Alert.alert(
      "Call " + OwnerName,
      "Call " + OwnerMob + " by pressing the 'Call' Button",
      [{ text: "Cancel", style: "cancel" },
      { text: "Call", onPress: () => call(args).catch(console.error) }],
      { cancelable: true }
    );
  };

  handlePicker = (date) => {
    var Day = new Date().getDate()
    var Month = new Date().getMonth() + 1
    var Year = new Date().getFullYear()

    var today = Month + "/" + Day + "/" + Year
    var selectedday = moment(date).format("MM/DD/YYYY")

    var datum1 = Date.parse(today);
    var todayTimestamp = datum1 / 1000;

    var datum2 = Date.parse(selectedday);
    var chosenTimestamp = datum2 / 1000;

    if (chosenTimestamp > todayTimestamp) {
      alert("Invalid Date"), this.setState({ VisibleDateTimePickerModal: false })
    }
    else {
      this.setState({
        chosenDate: moment(date).format("DD/MM/YYYY"), VisibleDateTimePickerModal: false
      })
    }
  }

  hidePicker = () => {
    this.setState({ VisibleDateTimePickerModal: false })
  }

  showPicker = () => {
    this.setState({ VisibleDateTimePickerModal: true })
  }

  render() {
    return (
      <Container>
        <SafeAreaView style={{ flex: 1 }}>
          <View style={{ flex: 0.1, backgroundColor: '#229CE3', justifyContent: (Platform.OS === "android") ? "center" : 'flex-start' }}>
            <View style={{ flexDirection: 'row', marginHorizontal: responsiveWidth(2), marginTop: responsiveWidth(6), alignItems: 'center' }}>
              <View style={{ flex: 0.6, flexDirection: 'row', alignItems: 'center', }}>
                <TouchableOpacity style={{ marginHorizontal: responsiveWidth(2), height: responsiveHeight(4), width: responsiveWidth(8) }} onPress={() => { this.props.navigation.goBack() }}>
                  <Icon name="md-arrow-back" type="Ionicons" style={{ color: '#fff' }} />
                </TouchableOpacity>
                <Text style={Styles.BoldWhiteText}>History</Text>
              </View>
              <View style={{ flex: 0.4, alignItems: 'flex-end' }}>
                {this.showDate()}
              </View>
            </View>
          </View>
          <View style={{ flex: 0.05, alignItems: 'center', justifyContent: 'center', flexDirection: 'row' }}>
            <View style={{ width: responsiveWidth(80), borderBottomWidth: 2, marginHorizontal: responsiveWidth(2) }}>
              <Text style={Styles.boldBlackText}>{this.state.chosenDate}</Text>
            </View>
            <TouchableOpacity onPress={this.showPicker}>
              <Icon name="calendar" type="AntDesign" />
            </TouchableOpacity>

            <DateTimePicker
              isVisible={this.state.VisibleDateTimePickerModal}
              onConfirm={this.handlePicker}
              onCancel={this.hidePicker}
              mode={'date'} />
          </View>
          <View style={{ flex: 0.85, backgroundColor: '#F0F0F1', alignItems: 'center' }}>
            <ScrollView showsVerticalScrollIndicator={false}>
              {this.renderItems()}
            </ScrollView>
          </View>
        </SafeAreaView>
      </Container>
    );
  }
}

