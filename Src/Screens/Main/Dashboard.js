import React, { Component } from 'react';
import { View, Text, ImageBackground, TouchableOpacity, Image, Dimensions } from 'react-native';
import { Container, Content, Card } from 'native-base';
import { responsiveHeight, responsiveWidth, responsiveFontSize } from 'react-native-responsive-dimensions';
import Styles from '../../Components/Styles/Styles';

class Dashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      totalJobs: 9,
      jobsDone: 4,
      garageName: 'ABC Garage',
      garageEmail: 'helloworld@gmail.com',
      garageAddress: '#123, Abc Nagar,Mohali',
      showdate: true
    };
  }

  showDate() {
    var Day = new Date().getDate()
    var Month = new Date().getMonth() + 1
    var Year = new Date().getFullYear()

    if (this.state.showdate == true) {
      return (
        <View style={{ marginTop: responsiveWidth(6) }}>
          <Text style={Styles.intermediateBoldText}>Today : {Day}/{Month}/{Year}</Text>
        </View>
      )
    }
  }
  render() {
    return (
      <ImageBackground source={require('../../Components/Images/BG/Back.jpg')} style={{ flex: 1 }}>
        <View style={{ flex: 0.2, padding: responsiveWidth(4) }}>
          <Text style={Styles.HeaderText}>{this.state.garageName}</Text>
          <Text style={Styles.BoldWhiteText}>{this.state.garageEmail}</Text>
          <Text style={Styles.BoldWhiteText}>{this.state.garageAddress}</Text>
          {this.showDate()}
        </View>
        <View style={{ flex: 0.7, alignItems: 'center', justifyContent: 'center' }}>
          <View style={{ flexDirection: 'row', marginBottom: responsiveWidth(8) }}>
            <TouchableOpacity style={Styles.DashboardMainCard} onPress={() => { this.props.navigation.navigate("TodayAppointments") }}>
              <Image source={require('../../Components/Images/Icons/TodayAppointment.png')} style={Styles.DashboardMainIcon} />
              <Text style={{
                color: '#fff',
                fontWeight: 'bold',
                fontSize: responsiveFontSize(2.2),
                textAlign: 'center'
              }}>Today's Appointments</Text>
            </TouchableOpacity>
            <TouchableOpacity style={Styles.DashboardMainCard} onPress={() => { this.props.navigation.navigate("AllAppointments") }}>
              <Image source={require('../../Components/Images/Icons/AllAppointment.png')} style={Styles.DashboardMainIcon} />
              <Text style={Styles.BoldWhiteText}>All Appointments</Text>
            </TouchableOpacity>
          </View>
          <View style={{ flexDirection: 'row' }}>
            <TouchableOpacity style={Styles.DashboardMainCard} onPress={() => { this.props.navigation.navigate("JobStatus") }}>
              <Image source={require('../../Components/Images/Icons/JobStatus.png')} style={Styles.DashboardMainIcon} />
              <Text style={Styles.BoldWhiteText}>Job Status</Text>
            </TouchableOpacity>
            <TouchableOpacity style={Styles.DashboardMainCard} onPress={() => { this.props.navigation.navigate("History") }}>
              <Image source={require('../../Components/Images/Icons/History.png')} style={Styles.DashboardMainIcon} />
              <Text style={Styles.BoldWhiteText}>History</Text>
            </TouchableOpacity>
          </View>
        </View>
        <View style={{ flex: 0.1, padding: responsiveWidth(3) }}>
          <Card style={{ flex: 1, backgroundColor: '#229CE3', borderRadius: 20, justifyContent: 'center', paddingHorizontal: responsiveWidth(3) }}>
            <Text style={{ color: '#fff', fontSize: responsiveFontSize(3), fontWeight: 'bold' }}>Jobs Done: {this.state.jobsDone}/{this.state.totalJobs} </Text>
          </Card>
        </View>
      </ImageBackground>
    );
  }
}

export default Dashboard;
