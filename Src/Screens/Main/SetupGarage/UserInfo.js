import React, { Component } from 'react';
import { View, Text, ImageBackground, Dimensions, Image, KeyboardAvoidingView ,AsyncStorage} from 'react-native';
import { Container, Card, Spinner ,Toast} from 'native-base';
import Styles from '../../../Components/Styles/Styles';
import { responsiveWidth, responsiveHeight,responsiveFontSize } from 'react-native-responsive-dimensions';
import { TextInput, TouchableOpacity } from 'react-native-gesture-handler';

var height = Dimensions.get("screen").height
var width = Dimensions.get("screen").width

class UserInfo extends Component {
    constructor(props) {
        super(props);
        this.state = {
            OwnerName: '',
            OwnerEmail: '',
            OwnerMobile: '',
            OwnerAadhaar: '',
            ErrorMessage: '',
            emailStatus:2,
            emailVerified:false,
            flexView:false,
            one: "",
            two: "",
            three: "",
            four: "",
            otp: "",
            loading:false,
            loader:false,
            garageDetails:[],
        };
        this.getuserData();
    }

    getuserData = async() => {
        const uid = await AsyncStorage.getItem('uid');
        //console.log(uid)
        try{
            fetch("http://yoyorooms.in/fisso_gargage/appapi/getUserDetails.php", 
                {
                    method: 'POST',
                    headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({
                              u_id :uid,
                            }),     
                })
                .then(response => response.json())
                .then(response2 => {
                //console.log(response2)
                  
                    if(response2.status==1){
                      this.setState({OwnerName:response2.userData.UserName,OwnerEmail:response2.userData.UserEmail,
                                    emailStatus:response2.userData.email_status})
                      if(response2.userData.email_status == 1){
                          this.setState({emailVerified:true})
                      }
                      else{
                        this.setState({emailVerified:false})
                      }
                     
                    }
                    else{
                      Toast.show({
                        text: "Something Went Wrong!",
                        textStyle: { textAlign: "center", color: '#111' },
                        duration: 2000,
                        position: 'top',
                        style: { borderRadius: 10, margin: responsiveWidth(10), backgroundColor: 'rgba(255,255,255,0.8)' }
                      });
                      this.props.navigation.navigate("auth");
                    }
                }).catch((error) =>{
                      console.log(error)
                });
            
        }
        catch(error){
              console.log(error)
        }
    }
    sendotp = () => {
        this.setState({loading:true})
        try{
            fetch("http://yoyorooms.in/fisso_gargage/appapi/mailsend.php", 
                {
                    method: 'POST',
                    headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({
                                u_email :this.state.OwnerEmail,
                            }),     
                })
                .then(response => response.json())
                .then(response2 => {
                //console.log(response2)
                  
                    if(response2.otpMailSend==1){
                      this.setState({loading:false,flexView:true,otp:response2.userData.otp})
                      
                     
                    }
                    else{
                      this.setState({loading:false})
                      Toast.show({
                        text: "Something Went Wrong!",
                        textStyle: { textAlign: "center", color: '#111' },
                        duration: 2000,
                        position: 'top',
                        style: { borderRadius: 10, margin: responsiveWidth(10), backgroundColor: 'rgba(255,255,255,0.8)' }
                      });
                      this.props.navigation.navigate("auth");
                    }
                }).catch((error) =>{
                      this.setState({loading:false})
                      console.log(error)
                });
            
        }
        catch(error){
              this.setState({loading:false})
              console.log(error)
        }
    }
    verifyotp = () => {
        let OTP = parseInt(this.state.one + this.state.two + this.state.three + this.state.four)
        this.setState({loader:true})
        if (this.state.otp == OTP) 
        {   
            try{
                fetch("http://yoyorooms.in/fisso_gargage/appapi/verify_email.php", 
                    {
                        method: 'POST',
                        headers: {
                            Accept: 'application/json',
                            'Content-Type': 'application/json',
                        },
                        body: JSON.stringify({
                                  u_email :this.state.OwnerEmail,
                                }),     
                    })
                    .then(response => response.json())
                    .then(response2 => {
                    //console.log(response2)
                      
                        if(response2==1){
                          this.setState({loader:false,emailVerified:true,emailStatus:1})
                          Toast.show({
                            text: "Email Verified Successfully",
                            textStyle: { textAlign: "center", color: '#111' },
                            duration: 2000,
                            position: 'top',
                            style: { borderRadius: 10, margin: responsiveWidth(10), backgroundColor: 'rgba(255,255,255,0.8)' }
                          });
                         
                        }
                        else{
                          this.setState({loading:false})
                          Toast.show({
                            text: "Something Went Wrong!",
                            textStyle: { textAlign: "center", color: '#111' },
                            duration: 2000,
                            position: 'top',
                            style: { borderRadius: 10, margin: responsiveWidth(10), backgroundColor: 'rgba(255,255,255,0.8)' }
                          });
                        }
                    }).catch((error) =>{
                          this.setState({loader:false})
                          console.log(error)
                    });
                
            }
            catch(error){
                  this.setState({loader:false})
                  console.log(error)
            }
        }
        else 
        {   
            this.setState({loader:false})
            Toast.show({
                  text: "You enetered a wrong OTP!",
                  textStyle: { textAlign: "center", color: '#111' },
                  duration: 2000,
                  position: 'top',
                  style: { borderRadius: 10, margin: responsiveWidth(10), backgroundColor: 'rgba(255,255,255,0.8)' }
                }); 
        }
    }  
    
    verifyDetails() {

        let { OwnerName, OwnerEmail, OwnerMobile, OwnerAadhaar } = this.state
        var mobileFormat = /^[0]?[6789]\d{9}$/;
        var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        var AadharFormat = /^\d{4}\d{4}\d{4}$/
        if (OwnerName == "" || OwnerEmail == "" || OwnerMobile == "" || OwnerAadhaar == "") {
            this.setState({ ErrorMessage: "Please fill all the details" })
        }
        else {
            if (OwnerName.length >= 3) {
                if (OwnerEmail.match(mailformat)) {
                    if (OwnerMobile.match(mobileFormat) && OwnerMobile.length == 10) {
                        if (OwnerAadhaar.match(AadharFormat)) {
                            this.state.garageDetails.push({OwnerDetails:{ownerName:this.state.OwnerName,ownerEmail:this.state.OwnerEmail,
                                                        ownerMobile:this.state.OwnerMobile,ownerAAdhar:this.state.OwnerAadhaar}})
                            this.props.navigation.navigate("GarageInfo",{garageDetails:this.state.garageDetails})
                        }
                        else { this.setState({ ErrorMessage: "Enter Aadhaar Number in correct Format (i.e. 1111 2222 3333)" }) }
                    }
                    else { this.setState({ ErrorMessage: "Enter correct Mobile Number" }) }


                }
                else { this.setState({ ErrorMessage: "Enter correct Email (i.e. abc@example.com)" }) }
            }
            else { this.setState({ ErrorMessage: "Enter Full Name" }) }
        }
        setTimeout(() => { this.setState({ ErrorMessage: '' }) }, 4000)
    }
    render() {
        return (
            <Container>
                {(this.state.emailVerified) && (this.state.emailStatus==1) &&
                <ImageBackground source={require('../../../Components/Images/BG/Back.jpg')}
                    style={{ height: height, width: width, alignItems: 'center', justifyContent: 'center', }}>
                    <KeyboardAvoidingView behavior="padding">
                        <Card style={Styles.GarageSetupCustomCard}>
                            <View style={{backgroundColor: '#229CE3', borderTopLeftRadius: 10, borderTopRightRadius: 10,
                                        paddingHorizontal: responsiveWidth(3), justifyContent: 'center',paddingVertical:responsiveHeight(1) }}>
                                <Text style={Styles.HeaderText}>Owner Information</Text>
                                <Text style={Styles.BoldWhiteText}>Please Enter Owner Information</Text>
                            </View>
                            <View style={{ flex: 0.80, alignItems: 'center', justifyContent: 'center' }}>
                                <View style={{ marginBottom: responsiveWidth(4), marginVertical: responsiveHeight(2) }}>
                                    <Text>Owner Name</Text>
                                    <TextInput style={Styles.GarageSetupCustomInputs}
                                        editable={false}
                                        placeholder="Enter Full Name"
                                        value={this.state.OwnerName}
                                    />
                                </View>
                                <View style={{ marginBottom: responsiveWidth(4), marginVertical: responsiveHeight(2) }}>
                                    <Text>Owner Email</Text>
                                    <TextInput style={Styles.GarageSetupCustomInputs}
                                        editable={false}
                                        placeholder="Enter Email"
                                        value={this.state.OwnerEmail}/>
                                </View>
                                <View style={{ marginBottom: responsiveWidth(4), marginVertical: responsiveHeight(2) }}>
                                    <Text>Owner Mobile</Text>
                                    <TextInput style={Styles.GarageSetupCustomInputs}
                                        keyboardType="number-pad"
                                        placeholder="Enter Mobile Number"
                                        maxLength={10}
                                        value={this.state.OwnerMobile}
                                        returnKeyType="next"
                                        onChangeText={(OwnerMobile) => { this.setState({ OwnerMobile: OwnerMobile }) }}
                                        onSubmitEditing={() => { this.refs.Aadhar.focus() }}
                                        ref={'Mobile'} />
                                </View>
                                <View style={{ marginBottom: responsiveWidth(4), marginVertical: responsiveHeight(2) }}>
                                    <Text>Owner Aadhar Number</Text>
                                    <TextInput style={Styles.GarageSetupCustomInputs}
                                        keyboardType="number-pad"
                                        placeholder="Enter Aadhar Number"
                                        maxLength={12}
                                        value={this.state.OwnerAadhaar}
                                        returnKeyType="go"
                                        ref={'Aadhar'}
                                        onChangeText={(OwnerAadhaar) => { this.setState({ OwnerAadhaar: OwnerAadhaar }) }}
                                        onSubmitEditing={() => { this.verifyDetails() }} />
                                </View>
                            </View>
                            <View style={{ flex: 0.15, alignItems: 'center', justifyContent: 'center' }}>
                                <Text style={{ color: '#f11',marginBottom:responsiveHeight(2) }}>{this.state.ErrorMessage}</Text>
                                <View style={{ flexDirection: 'row',marginTop:responsiveHeight(3) }}>
                                    {/* <TouchableOpacity style={Styles.BlueBorderButton}>
                                        <Text style={Styles.BoldBlueText}>Back</Text>
                                    </TouchableOpacity> */}
                                    <TouchableOpacity style={Styles.normalButton} onPress={() => { this.verifyDetails() }}>
                                        <Text style={Styles.BoldWhiteText}>Next</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </Card>
                    </KeyboardAvoidingView>
                </ImageBackground>
                }
                {(!this.state.emailVerified) && (this.state.emailStatus==0) &&
                <View style={{display:this.state.flexView?"none":"flex" ,flex: 1, backgroundColor: '#229CE3', alignItems: 'center', justifyContent: 'center', }}>
                    <Card style={{ width: responsiveWidth(80), height: "auto", borderRadius: 15, padding: responsiveWidth(5) }}>
                        <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', }}>
                            <View style={{ flex: 1 }}>
                                <Text style={{ color: '#229CE3', fontSize: responsiveFontSize(3), fontWeight: 'bold' }}>
                                    Send OTP </Text>
                            </View>
                        </View>
                        <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', paddingVertical:responsiveHeight(1)}}>
                            <View style={{ flex: 1 }}>
                                <Text style={{ color: '#888', fontSize: responsiveFontSize(1.8), fontWeight: 'bold' }}>
                                    Send OTP to registered email</Text>
                            </View>
                        </View>
                        <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', paddingVertical:responsiveHeight(1)}}>
                            <View style={{ flex: 1 }}>
                                <TextInput style={{borderWidth:1,paddingHorizontal:responsiveWidth(3),borderColor:"#229CE3",
                                                borderRadius:10,}}
                                    placeholder="Enter Your Email"
                                    placeholderTextColor="#fff"
                                    keyboardType="email-address"
                                    value={this.state.OwnerEmail}
                                    returnKeyType="next"
                                    ref={'email'}
                                    autoCorrect={false}
                                    editable={false}/>
                            </View>
                        </View>
                        <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center',marginTop:responsiveHeight(3), 
                                    paddingVertical:responsiveHeight(1),paddingHorizontal:responsiveWidth(5)}}>
                            <TouchableOpacity style={{ paddingVertical: responsiveWidth(2),
                                                    borderWidth: 2,
                                                    borderColor: '#229CE3',
                                                    borderRadius: 10,
                                                    alignItems: 'center', justifyContent: 'center',
                                                    width: responsiveWidth(35),
                                                    marginHorizontal: responsiveWidth(3),
                                                    height:responsiveHeight(6)}} 
                                    onPress={() => this.props.navigation.navigate("SignUp")}>
                                <Text style={Styles.BoldBlueText}>Change Email</Text>
                            </TouchableOpacity>
                            {!this.state.loading &&
                                <TouchableOpacity style={{ paddingVertical: responsiveWidth(2),
                                                        borderWidth: 2,
                                                        borderColor: '#229CE3',
                                                        borderRadius: 10,
                                                        alignItems: 'center', justifyContent: 'center',
                                                        width: responsiveWidth(35),
                                                        marginHorizontal: responsiveWidth(3),
                                                        height:responsiveHeight(6)}} 
                                        onPress={() => this.sendotp()}>
                                    <Text style={Styles.BoldBlueText}>Send OTP</Text>
                                </TouchableOpacity>
                            }
                            {this.state.loading &&
                                <TouchableOpacity style={{ paddingVertical: responsiveWidth(2),
                                                        borderWidth: 2,
                                                        borderColor: '#229CE3',
                                                        borderRadius: 10,
                                                        alignItems: 'center', justifyContent: 'center',
                                                        width: responsiveWidth(35),
                                                        marginHorizontal: responsiveWidth(3),
                                                        height:responsiveHeight(6)}} 
                                        onPress={() => this.sendotp()}>
                                    <Spinner size="small" color="#229CE3"/>
                                </TouchableOpacity>
                            }
                        </View>
                        
                    </Card>
                </View>
                }
                {(!this.state.emailVerified) && (this.state.emailStatus==0) &&
                <View style={{ display:this.state.flexView?"flex":"none",flex: 1, backgroundColor: '#229CE3', alignItems: 'center', justifyContent: 'center', }}>
                    <Card style={{ width: responsiveWidth(80), height: "auto", borderRadius: 15, padding: responsiveWidth(5) }}>
                        <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', }}>
                            <View style={{ flex: 1 }}>
                                <Text style={{ color: '#229CE3', fontSize: responsiveFontSize(3), fontWeight: 'bold' }}>
                                    Send OTP </Text>
                            </View>
                        </View>
                        <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', paddingVertical:responsiveHeight(1)}}>
                            <View style={{ flex: 1 }}>
                            <Text style={{ color: '#888' }}>Enter the OTP sent to : <Text style={{ color: '#229CE3' }}>{this.state.OwnerEmail}</Text></Text>
                            </View>
                        </View>
                        <View style={{ marginVertical: responsiveWidth(7), flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                            <View style={styles.OtpBox}>
                                <TextInput style={styles.OtpInput}
                                    keyboardType="number-pad"
                                    onChangeText={(one) => { this.setState({ one: one }), this.refs.two.focus() }} />
                            </View>

                            <View style={styles.OtpBox}>
                                <TextInput style={styles.OtpInput}
                                    keyboardType="number-pad"
                                    onChangeText={(two) => { this.setState({ two: two }), this.refs.three.focus() }}
                                    ref={'two'} />
                            </View>

                            <View style={styles.OtpBox}>
                                <TextInput style={styles.OtpInput}
                                    keyboardType="number-pad"
                                    onChangeText={(three) => { this.setState({ three: three }), this.refs.four.focus() }}
                                    ref={'three'} />
                            </View>

                            <View style={styles.OtpBox}>
                                <TextInput style={styles.OtpInput}
                                    keyboardType="number-pad"
                                    onChangeText={(four) => { this.setState({ four: four }) }}
                                    ref={'four'} />
                            </View>
                        </View>

                        <View style={{alignItems: 'center', justifyContent:"center" }}>
                            {!this.state.loader &&
                                <TouchableOpacity style={{ paddingVertical: responsiveWidth(2),
                                                        borderWidth: 2,
                                                        borderColor: '#229CE3',
                                                        borderRadius: 10,
                                                        alignItems: 'center', justifyContent: 'center',
                                                        width: responsiveWidth(40),
                                                        marginHorizontal: responsiveWidth(3),
                                                        height:responsiveHeight(6)}} 
                                        onPress={() => this.verifyotp()}>
                                    <Text style={Styles.BoldBlueText}>Verify</Text>
                                </TouchableOpacity>
                            }
                            {this.state.loader &&
                                <TouchableOpacity style={{ paddingVertical: responsiveWidth(2),
                                                        borderWidth: 2,
                                                        borderColor: '#229CE3',
                                                        borderRadius: 10,
                                                        alignItems: 'center', justifyContent: 'center',
                                                        width: responsiveWidth(40),
                                                        marginHorizontal: responsiveWidth(3),
                                                        height:responsiveHeight(6)}} 
                                        onPress={() => this.verifyotp()}>
                                    <Spinner size="small" color="#229CE3" />
                                </TouchableOpacity>
                            }
                        </View>
                        
                    </Card>
                </View>
                }
                {(!this.state.emailVerified) && (this.state.emailStatus==2) &&
                <View style={{ flex: 1, backgroundColor: '#229CE3', alignItems: 'center', justifyContent: 'center', }}>
                    <Spinner size="large" color="#fff"/>
                </View>
                }
            </Container>
        );
    }
}

export default UserInfo;

const styles = {
    OtpBox: {
        borderWidth: 1,
        height: responsiveHeight(5),
        width: responsiveWidth(10),
        marginHorizontal: responsiveWidth(2.5),
        borderRadius: 10,
        alignItems: 'center',
        justifyContent: 'center',
    },
    OtpInput: {
        width: responsiveWidth(4),
        fontSize: responsiveFontSize(2.5)
    }
}