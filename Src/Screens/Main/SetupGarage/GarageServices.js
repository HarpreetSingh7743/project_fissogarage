import React, { Component } from 'react';
import { View, Text, ImageBackground, Dimensions, Image, TouchableOpacity, ScrollView, TextInput,AsyncStorage } from 'react-native';
import { Container, Card, Icon, Radio ,Toast} from 'native-base';
import Styles from '../../../Components/Styles/Styles';
import { responsiveWidth, responsiveHeight } from 'react-native-responsive-dimensions';
import Spinner from "react-native-loading-spinner-overlay";

var height = Dimensions.get("screen").height
var width = Dimensions.get("screen").width

class GarageServices extends Component {
  constructor(props) {
    super(props);
    this.state = {
      radio1: false,
      radio2: true,
      loader:false,
      show2WheelServices: [],
      show4WheelServices: [],
      addservice2Wheel: '',
      addservice4Wheel: '',
      garageDetails: this.props.navigation.getParam('garageDetails', 'NO-garage-Selected'),
    };
  }

  renderScreen() {
    let { radio1, radio2 } = this.state
     if (radio1 == true && radio2 == false) {
      return (
        <View>
          <ScrollView>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <TextInput style={{
                width: responsiveWidth(57), borderWidth: 1, marginHorizontal: responsiveWidth(2.5),
                borderTopLeftRadius: 10, borderBottomLeftRadius: 10,paddingHorizontal:responsiveWidth(1.5)
              }}
                value={this.state.addservice4Wheel}
                onChangeText={(addservice4Wheel) => { this.setState({ addservice4Wheel: addservice4Wheel }) }}
                returnKeyType="go"
                onSubmitEditing={() => { this.add4WheelServices() }} />
              <TouchableOpacity style={{
                backgroundColor: "#229CE3", paddingHorizontal: responsiveWidth(6),
                paddingVertical: responsiveWidth(1.3), borderTopRightRadius: 10, borderBottomRightRadius: 10
              }}
                onPress={() => { this.add4WheelServices() }}>
                <Text style={Styles.BoldWhiteText}>Add</Text>
              </TouchableOpacity>
            </View>
            {this.edit4WheelServices()}


          </ScrollView>
        </View>
      )
    }
    else if (radio1 == false && radio2 == true ) {
      return (
        <View>
          <ScrollView>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <TextInput style={{
                width: responsiveWidth(57), borderWidth: 1, marginHorizontal: responsiveWidth(2.5),
                borderTopLeftRadius: 10, borderBottomLeftRadius: 10,paddingHorizontal:responsiveWidth(1.5)
              }}
                value={this.state.addservice2Wheel}
                onChangeText={(addservice2Wheel) => { this.setState({ addservice2Wheel: addservice2Wheel }) }}
                returnKeyType="go"
                onSubmitEditing={() => { this.add2WheelServices() }} />
              <TouchableOpacity style={{
                backgroundColor: "#229CE3", paddingHorizontal: responsiveWidth(6),
                paddingVertical: responsiveWidth(1.3), borderTopRightRadius: 10, borderBottomRightRadius: 10
              }}
                onPress={() => { this.add2WheelServices() }}>
                <Text style={Styles.BoldWhiteText}>Add</Text>
              </TouchableOpacity>
            </View>
            {this.edit2WheelServices()}

          </ScrollView>
        </View>
      )
    }
  }

  //*******************    DELETE SERVICES   *********************** */
  delete2WheelServices = (t) => {
    var arr = this.state.show2WheelServices
    var pos = arr.indexOf(t);
    arr.splice(pos, 1)
    this.setState({ show2WheelServices: arr })
  }
  delete4WheelServices = (t) => {
    var arr = this.state.show4WheelServices
    var pos = arr.indexOf(t);
    arr.splice(pos, 1)
    this.setState({ show4WheelServices: arr })
  }
  //****************************************** */

  /************************************************************/
  add2WheelServices() {
    var arr = this.state.show2WheelServices;
    if (this.state.addservice2Wheel == "") {
      alert("Write Service Name")
    }
    else {
      if (arr.indexOf(this.state.addservice2Wheel) > 0) {
        alert("Already Exists !")
      }
      else {
        arr.push(this.state.addservice2Wheel);
        this.setState({ show2WheelServices: arr, addservice2Wheel: '' })
      }
    }
  }

  add4WheelServices() {
    var arr = this.state.show4WheelServices;
    if (this.state.addservice4Wheel == "") {
      alert("Write Service Name")
    }
    else {
      if (arr.indexOf(this.state.addservice4Wheel) > 0) {
        alert("Already Exists !")
      }
      else {
        arr.push(this.state.addservice4Wheel)
        this.setState({ show4WheelServices: arr, addservice4Wheel: '' })
      }
    }
  }
  /***********************    EDIT SERVICES    *************************************/

  edit2WheelServices() {
    return this.state.show2WheelServices.map(t => {
      return (
        <View key={t}>
          <View style={{ marginBottom: responsiveWidth(2), flexDirection: 'row', alignItems: 'center' }}>
            <View style={{ flex: 0.8 }}>
              <Text style={Styles.boldBlackText}>{t}</Text>
            </View>
            <View style={{ flex: 0.2, alignItems: 'center' }}>
              <TouchableOpacity onPress={() => { this.delete2WheelServices(t) }}>
                <Icon name="circle-with-cross" type="Entypo" style={{ color: '#f11' }} />
              </TouchableOpacity>
            </View>

          </View>
        </View>
      )
    })
  }

  edit4WheelServices() {
    return this.state.show4WheelServices.map(t => {
      return (
        <View key={t}>
          <View style={{ marginBottom: responsiveWidth(2), flexDirection: 'row', alignItems: 'center' }}>
            <View style={{ flex: 0.8 }}>
              <Text style={Styles.boldBlackText}>{t}</Text>
            </View>
            <View style={{ flex: 0.2, alignItems: 'center' }}>
              <TouchableOpacity onPress={() => { this.delete4WheelServices(t) }}>
                <Icon name="circle-with-cross" type="Entypo" style={{ color: '#f11' }} />
              </TouchableOpacity>
            </View>

          </View>
        </View>
      )
    })
  }
  /************************************************************/
  verifyDetails = async () => {
    let { show2WheelServices, show4WheelServices } = this.state
    if (show2WheelServices == "" && show4WheelServices == "") {
      alert("Add atleast one Service to 4 Wheeler or 2 Wheeler")
    }
    else {
      await this.state.garageDetails.push({
        vechileServices: {
              twoWheelers:this.state.show2WheelServices,
              fourWheeler:this.state.show4WheelServices
        }, 
      })
      this.addGarage();
      //console.log(this.state.garageDetails)
      //alert("Garage Added Sucessfully")
    }
  }

  addGarage = () => {
    //console.log(this.state.garageDetails[1].GarageInfo);
    try{
      this.setState({loader:true});
      fetch("http://yoyorooms.in/fisso_gargage/appapi/addgarage.php", 
          {
              method: 'POST',
              headers: {
                  Accept: 'application/json',
                  'Content-Type': 'application/json',
              },
              body: JSON.stringify({
                        ownerDetails : this.state.garageDetails[0].OwnerDetails,
                        garageInfo : this.state.garageDetails[1].GarageInfo,
                        selectedVechiles : this.state.garageDetails[2].vechileSelected,
                        selectServices : this.state.garageDetails[3].vechileServices
                      }),     
          })
          .then(response => response.json())
          .then(response2 => {
          //console.log(response2)
            
              if(response2==1){
                this.setState({loader:false,})
                Toast.show({
                  text: "Gagage Added Successfully",
                  textStyle: { textAlign: "center", color: '#111' },
                  duration: 2000,
                  position: 'top',
                  style: { borderRadius: 10, margin: responsiveWidth(10), backgroundColor: 'rgba(255,255,255,0.8)' }
                });
                
                AsyncStorage.setItem('uStatus',"1");
                this.props.navigation.navigate("main");
               
              }
              else{
                this.setState({loader:false})
                Toast.show({
                  text: "Something Went Wrong!",
                  textStyle: { textAlign: "center", color: '#111' },
                  duration: 2000,
                  position: 'top',
                  style: { borderRadius: 10, margin: responsiveWidth(10), backgroundColor: 'rgba(255,255,255,0.8)' }
                });
              }
          }).catch((error) =>{
                this.setState({loader:false})
                console.log(error)
          });
      
    }
    catch(error){
          this.setState({loader:false})
          console.log(error)
    }
}
  /************************************************************************************** START*/
  render() {
    //  var details = this.props.navigation.getParam('garageDetails', 'NO-garage-Selected');
    // console.log(details)
    return (
      <Container>
        <ImageBackground source={require('../../../Components/Images/BG/Back.jpg')}
          style={{ height: height, width: width, alignItems: 'center', justifyContent: 'center', }}>
          <Card style={Styles.GarageSetupCustomCard}>
            <View style={{ flex: 0.15, backgroundColor: '#229CE3', borderTopLeftRadius: 10, borderTopRightRadius: 10, paddingHorizontal: responsiveWidth(2), justifyContent: 'center' }}>
              <Text style={Styles.HeaderText}>Garage Services</Text>
              <Text style={Styles.BoldWhiteText}>Enter services provided by your Garage</Text>
            </View>
            <View style={{ flexDirection: 'row', margin: responsiveWidth(2) }}>
              <Text style={Styles.BoldBlueText}>Vehicles Type :  </Text>
              <View style={{ marginLeft: responsiveWidth(2), flexDirection: 'row' }}>
                <TouchableOpacity style={{ flexDirection: 'row' }}
                  onPress={() => { this.setState({ radio2: true, radio1: false }) }}>
                  <Radio selected={this.state.radio2}
                    color={"#229CE3"}
                    selectedColor={'#229CE3'}
                    onPress={() => { this.setState({ radio2: true, radio1: false }) }} />
                  <Text style={{ marginLeft: responsiveWidth(1) }}>2 Wheeler</Text>
                </TouchableOpacity>

                <TouchableOpacity style={{ flexDirection: 'row', marginLeft: responsiveWidth(6) }}
                  onPress={() => { this.setState({ radio1: true, radio2: false }) }}>
                  <Radio selected={this.state.radio1}
                    color={"#229CE3"}
                    selectedColor={'#229CE3'}
                    onPress={() => { this.setState({ radio1: true, radio2: false }) }} />
                  <Text style={{ marginLeft: responsiveWidth(1) }}>4 Wheeler</Text>
                </TouchableOpacity>
              </View>
            </View>
            <View style={{ flex: 0.75, alignItems: 'center' }}>
              {this.renderScreen()}
            </View>
            <View style={{ flex: 0.1, alignItems: 'center', justifyContent: 'center', flexDirection: 'row' }}>
              <TouchableOpacity style={Styles.BlueBorderButton} onPress={() => { this.props.navigation.goBack() }}>
                <Text style={Styles.BoldBlueText}>Back</Text>
              </TouchableOpacity>
              <TouchableOpacity style={Styles.normalButton} onPress={() => { this.verifyDetails() }}>
                <Text style={Styles.BoldWhiteText}>Next</Text>
              </TouchableOpacity>
            </View>
          </Card>
        </ImageBackground>
        <Spinner
        visible={this.state.loader}
        textContent="Adding Garage..."

        />
      </Container>
    );
  }
}

export default GarageServices;
