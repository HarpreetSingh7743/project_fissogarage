import React, { Component } from 'react';
import { View, Text, ImageBackground, Dimensions, Image, KeyboardAvoidingView, Modal } from 'react-native';
import { Container, Card, Icon, CheckBox } from 'native-base';
import Styles from '../../../Components/Styles/Styles';
import { responsiveWidth, responsiveHeight } from 'react-native-responsive-dimensions';
import { TextInput, TouchableOpacity, ScrollView } from 'react-native-gesture-handler';
import DateTimePicker from 'react-native-modal-datetime-picker';
import OpenMapModal from '../../../Components/CustomModals/OpenMapModal'
import moment from 'moment';

var height = Dimensions.get("screen").height
var width = Dimensions.get("screen").width

class GarageInfo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      VisibleDateTimePickerModal: false,
      VisibleDateTimePickerModal2: false,
      visibleOpenMapModal: false,
      GarageName: '',
      GarageEmail: '',
      GarageAddress: '',
      GarageZIPCode: '',
      GaragePhone: '',
      latitude: '',
      longitude: '',
      lat: 0,
      lng: 0,
      GarageOpenDays: [],
      GarageOpenTime: '',
      GarageCloseTime: '',
      garageDetails: this.props.navigation.getParam('garageDetails', 'NO-garage-Selected'),
      check0:false,
      check1: false,
      check2: false,
      check3: false,
      check4: false,
      check5: false,
      check6: false,
      ErrorMessage: '',
      location: null,
      errorMessage: null
    };
  }
  findCurrentLocation = () => {
    navigator.geolocation.getCurrentPosition(
      position => {
        const latitude = JSON.stringify(position.coords.latitude);
        const longitude = JSON.stringify(position.coords.longitude);
        //console.log(latitude)
        this.setState({
          latitude,
          longitude
        });
        this.setState({ visibleOpenMapModal: true, lat: latitude, lng: longitude, })
      },
      { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 }
    );
  }

  verifyDetails() {
    let { GarageName, GarageEmail, GarageAddress, GarageZIPCode, GaragePhone, latitude, longitude, GarageOpenTime, GarageCloseTime } = this.state
    var mobileFormat = /^[0]?[6789]\d{9}$/;
    var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

    if (GarageName != "" || GarageEmail != "" || GarageAddress != "" || GarageZIPCode != "" || GaragePhone != "" || latitude != "" || longitude != "" || GarageOpenDays != "" || GarageOpenTime != "" || GarageCloseTime != "") {
      if (GarageName.length >= 3) {
        if (GarageEmail.match(mailformat)) {
          if (GarageAddress.length >= 5) {
            if (GarageZIPCode.length == 6) {
              if (GaragePhone.match(mobileFormat)) {
                var GarageOpenDays = []
                GarageOpenDays = this.state.GarageOpenDays
                if (this.state.check0 == true && GarageOpenDays.indexOf("Sun") < 0) { GarageOpenDays.push("Sun") }

                if (this.state.check1 == true && GarageOpenDays.indexOf("Mon") < 0) { GarageOpenDays.push("Mon") }

                if (this.state.check2 == true && GarageOpenDays.indexOf("Tue") < 0) { GarageOpenDays.push("Tue") }

                if (this.state.check3 == true && GarageOpenDays.indexOf("Wed") < 0) { GarageOpenDays.push("Wed") }

                if (this.state.check4 == true && GarageOpenDays.indexOf("Thu") < 0) { GarageOpenDays.push("Thu") }

                if (this.state.check5 == true && GarageOpenDays.indexOf("Fri") < 0) { GarageOpenDays.push("Fri") }

                if (this.state.check6 == true && GarageOpenDays.indexOf("Sat") < 0) { GarageOpenDays.push("Sat") }

                this.setState({ GarageOpenDays: GarageOpenDays })
                this.state.garageDetails.push({
                  GarageInfo: {
                    GarageName: this.state.GarageName,
                    GarageEmail: this.state.GarageEmail,
                    GarageAddress: this.state.GarageAddress,
                    GarageZIPCode: this.state.GarageZIPCode,
                    GaragePhone: this.state.GaragePhone,
                    latitude: this.state.latitude,
                    longitude: this.state.longitude,
                    GarageOpenDays: this.state.GarageOpenDays,
                    GarageOpenTime: this.state.GarageOpenTime,
                    closingTime: this.state.GarageCloseTime
                  }
                })
                this.props.navigation.navigate("GarageVehicles", { garageDetails: this.state.garageDetails })
              }
              else { this.setState({ ErrorMessage: 'Please enter correct Mobile Number' }) }
            }
            else { this.setState({ ErrorMessage: 'Please enter correct ZIP Code' }) }
          }
          else { this.setState({ ErrorMessage: 'Please enter Full Address' }) }
        }
        else { this.setState({ ErrorMessage: 'Please enter correct Email (i.e. abc@example.com)' }) }
      }
      else { this.setState({ ErrorMessage: 'Please enter Full Name' }) }
    }
    else { this.setState({ ErrorMessage: 'Please enter all details' }) }
    setTimeout(() => { this.setState({ ErrorMessage: '' }) }, 4000)
  }

  handlePicker = (time) => {
    this.setState({
      VisibleDateTimePickerModal: false,
      GarageOpenTime: moment(time).format("HH:mm")
    })
  }
  hidePicker = () => {
    this.setState({ VisibleDateTimePickerModal: false })
  }
  showPicker = () => {
    this.setState({ VisibleDateTimePickerModal: true })
  }

  handlePicker2 = (time) => {
    this.setState({
      VisibleDateTimePickerModal2: false,
      GarageCloseTime: moment(time).format("HH:mm")
    })
  }
  hidePicker2 = () => {
    this.setState({ VisibleDateTimePickerModal2: false })
  }
  showPicker2 = () => {
    this.setState({ VisibleDateTimePickerModal2: true })
  }

  render() {
    // var details = this.props.navigation.getParam('garageDetails', 'NO-garage-Selected');
    // console.log(details)
    return (
      <Container>
        <ImageBackground source={require('../../../Components/Images/BG/Back.jpg')}
          style={{ height: height, width: width, alignItems: 'center', justifyContent: 'center', }}>
          <KeyboardAvoidingView behavior="padding">
            <Card style={Styles.GarageSetupCustomCard}>
              <View style={{ flex: 0.15, backgroundColor: '#229CE3', borderTopLeftRadius: 10, borderTopRightRadius: 10, paddingHorizontal: responsiveWidth(2), justifyContent: 'center' }}>
                <Text style={Styles.HeaderText}>Garage Information</Text>
                <Text style={Styles.BoldWhiteText}>Enter Garage Information</Text>
              </View>
              <View style={{ flex: 0.76, alignItems: 'center', justifyContent: 'center' }}>
                <ScrollView showsVerticalScrollIndicator={false}>
                  <View style={{ marginBottom: responsiveWidth(4), marginVertical: responsiveHeight(2) }}>
                    <Text>Garage Name</Text>
                    <TextInput style={Styles.GarageSetupCustomInputs}
                      keyboardType="default"
                      placeholder="Enter garage Name"
                      returnKeyType="next"
                      onChangeText={(GarageName) => { this.setState({ GarageName: GarageName }) }}
                      onSubmitEditing={() => { this.refs.email.focus() }}
                    />
                  </View>
                  <View style={{ marginBottom: responsiveWidth(4), marginVertical: responsiveHeight(2) }}>
                    <Text>Garage Email</Text>
                    <TextInput style={Styles.GarageSetupCustomInputs}
                      keyboardType="email-address"
                      placeholder="Enter Email"
                      returnKeyType="next"
                      onChangeText={(GarageEmail) => { this.setState({ GarageEmail: GarageEmail }) }}
                      onSubmitEditing={() => { this.refs.address.focus() }}
                      ref={'email'} />
                  </View>

                  <View style={{ marginBottom: responsiveWidth(4), marginVertical: responsiveHeight(2) }}>
                    <Text>Garage Address</Text>
                    <TextInput style={Styles.GarageSetupCustomInputs}
                      keyboardType="default"
                      placeholder="Enter Garage Address"
                      returnKeyType="next"
                      onChangeText={(GarageAddress) => { this.setState({ GarageAddress: GarageAddress }) }}
                      onSubmitEditing={() => { this.refs.zip.focus() }}
                      ref={'address'} />
                  </View>

                  <View style={{ marginBottom: responsiveWidth(4), marginVertical: responsiveHeight(2) }}>
                    <Text>Zip Code</Text>
                    <TextInput style={Styles.GarageSetupCustomInputs}
                      keyboardType="number-pad"
                      placeholder="Enter ZIP Code"
                      maxLength={6}
                      returnKeyType="next"
                      onChangeText={(GarageZIPCode) => { this.setState({ GarageZIPCode: GarageZIPCode }) }}
                      onSubmitEditing={() => { this.refs.contact.focus() }}
                      ref={'zip'} />
                  </View>

                  <View style={{ marginBottom: responsiveWidth(4), marginVertical: responsiveHeight(2) }}>
                    <Text>Garage Phone Number</Text>
                    <TextInput style={Styles.GarageSetupCustomInputs}
                      keyboardType="number-pad"
                      placeholder="Enter Garage contact"
                      returnKeyType="next"
                      maxLength={10}
                      onChangeText={(GaragePhone) => { this.setState({ GaragePhone: GaragePhone }) }}
                      ref={'contact'}
                      onSubmitEditing={() => { alert("Select Location on Map") }} />
                  </View>

                  <View style={{ marginBottom: responsiveWidth(4), marginVertical: responsiveHeight(2) }}>
                    <Text>Set Garage Location</Text>
                    <TouchableOpacity onPress={() => this.findCurrentLocation()} style={{ flexDirection: 'row' }}>
                      <View style={{ width: responsiveWidth(70), height: "auto", borderBottomWidth: 1, borderBottomColor: '#229CE3' }}>
                        <Text>{this.state.latitude + "   " + this.state.longitude}</Text>
                      </View>
                      <TouchableOpacity onPress={() => this.findCurrentLocation()}>
                        <Icon name="location" type="Entypo" style={{ color: '#229CE3', marginLeft: responsiveWidth(3) }} />
                      </TouchableOpacity>
                    </TouchableOpacity>
                  </View>

                  <View style={{ marginBottom: responsiveWidth(4), }}>
                    <Text>Garage Open Days</Text>

                    <View style={{ flexDirection: 'row', marginTop: responsiveWidth(1) }}>
                      <CheckBox checked={this.state.check0} onPress={() => { this.setState({ check0: !this.state.check0 }) }} />
                      <Text style={{ marginLeft: responsiveWidth(3) }}>Sun</Text>
                    </View>
                    <View style={{ flexDirection: 'row', marginTop: responsiveWidth(1) }}>
                      <CheckBox checked={this.state.check1} onPress={() => { this.setState({ check1: !this.state.check1 }) }} />
                      <Text style={{ marginLeft: responsiveWidth(3) }}>Mon</Text>
                    </View>
                    <View style={{ flexDirection: 'row', marginTop: responsiveWidth(1) }}>
                      <CheckBox checked={this.state.check2} onPress={() => { this.setState({ check2: !this.state.check2 }) }} />
                      <Text style={{ marginLeft: responsiveWidth(3) }}>Tue</Text>
                    </View>
                    <View style={{ flexDirection: 'row', marginTop: responsiveWidth(1) }}>
                      <CheckBox checked={this.state.check3} onPress={() => { this.setState({ check3: !this.state.check3 }) }} />
                      <Text style={{ marginLeft: responsiveWidth(3) }}>Wed</Text>
                    </View>
                    <View style={{ flexDirection: 'row', marginTop: responsiveWidth(1) }}>
                      <CheckBox checked={this.state.check4} onPress={() => { this.setState({ check4: !this.state.check4 }) }} />
                      <Text style={{ marginLeft: responsiveWidth(3) }}>Thu</Text>
                    </View>
                    <View style={{ flexDirection: 'row', marginTop: responsiveWidth(1) }}>
                      <CheckBox checked={this.state.check5} onPress={() => { this.setState({ check5: !this.state.check5 }) }} />
                      <Text style={{ marginLeft: responsiveWidth(3) }}>Fri</Text>
                    </View>
                    <View style={{ flexDirection: 'row', marginTop: responsiveWidth(1) }}>
                      <CheckBox checked={this.state.check6} onPress={() => { this.setState({ check6: !this.state.check6 }) }} />
                      <Text style={{ marginLeft: responsiveWidth(3) }}>Sat</Text>
                    </View>
                  </View>

                  {/* First datetime modal here */}
                  <DateTimePicker
                    isVisible={this.state.VisibleDateTimePickerModal}
                    onConfirm={this.handlePicker}
                    onCancel={this.hidePicker}
                    mode={'time'}
                    is24Hour={false} />

                  {/* First datetime modal here */}
                  <DateTimePicker
                    isVisible={this.state.VisibleDateTimePickerModal2}
                    onConfirm={this.handlePicker2}
                    onCancel={this.hidePicker2}
                    mode={'time'}
                    is24Hour={false} />

                  {/* OpenMapModal modal is here */}
                  {this.state.visibleOpenMapModal &&
                    <OpenMapModal lat={this.state.lat} lng={this.state.lng} onAction={(val) => this.setState({ visibleOpenMapModal: val })}
                      onConfirm={(val, lat, lng) => { this.setState({ visibleOpenMapModal: val, latitude: lat, longitude: lng }) }} />}

                  <View style={{ marginBottom: responsiveWidth(4), marginVertical: responsiveHeight(2) }}>
                    <Text>Garage Opening Time</Text>
                    <TouchableOpacity onPress={this.showPicker} style={{ flexDirection: 'row' }}>
                      <View style={{ width: responsiveWidth(70), height: responsiveHeight(3), borderBottomWidth: 1, borderBottomColor: '#229CE3' }}>
                        <Text>{this.state.GarageOpenTime}</Text>
                      </View>
                      <TouchableOpacity onPress={this.showPicker}>
                        <Icon name="ios-clock" type="Ionicons" style={{ color: '#229CE3', marginLeft: responsiveWidth(3) }} />
                      </TouchableOpacity>
                    </TouchableOpacity>
                  </View>

                  <View style={{ marginBottom: responsiveWidth(4), marginVertical: responsiveHeight(2) }}>
                    <Text>Garage Closing Time</Text>
                    <TouchableOpacity onPress={() => { this.showPicker2() }} style={{ flexDirection: 'row' }}>
                      <View style={{ width: responsiveWidth(70), height: responsiveHeight(3), borderBottomWidth: 1, borderBottomColor: '#229CE3' }}>
                        <Text>{this.state.GarageCloseTime}</Text>
                      </View>
                      <TouchableOpacity onPress={() => { this.showPicker2() }}>
                        <Icon name="md-clock" style={{ color: '#229CE3', marginLeft: responsiveWidth(3) }} />
                      </TouchableOpacity>
                    </TouchableOpacity>
                  </View>

                </ScrollView>
                <Text style={{ color: '#f11' }}>{this.state.ErrorMessage}</Text>
              </View>
              <View style={{ flex: 0.09, alignItems: 'center', justifyContent: 'center', flexDirection: 'row' }}>
                <TouchableOpacity style={Styles.BlueBorderButton} onPress={() => { this.props.navigation.goBack() }}>
                  <Text style={Styles.BoldBlueText}>Back</Text>
                </TouchableOpacity>
                <TouchableOpacity style={Styles.normalButton} onPress={() => { this.verifyDetails() }}>
                  <Text style={Styles.BoldWhiteText}>Next</Text>
                </TouchableOpacity>
              </View>
            </Card>
          </KeyboardAvoidingView>
        </ImageBackground>
      </Container>
    );
  }
}

export default GarageInfo;
