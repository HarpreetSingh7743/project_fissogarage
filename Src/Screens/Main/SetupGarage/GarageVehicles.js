import React, { Component } from 'react';
import { View, Text, ImageBackground, Dimensions, Image, TouchableOpacity, ScrollView, Alert } from 'react-native';
import { Container, Card, Icon, Radio, CheckBox } from 'native-base';
import Styles from '../../../Components/Styles/Styles';
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions';

var height = Dimensions.get("screen").height
var width = Dimensions.get("screen").width

class GarageVehicles extends Component {
  constructor(props) {
    super(props);
    this.state = {
      garageDetails: this.props.navigation.getParam('garageDetails', 'NO-garage-Selected'),
      Selected2wheelers: [],
      Selected4wheelers: [],
      radio1: false,
      radio2: true,
      vechicleTypes: 'none',
      buttonText: 'Edit',

      //data1 contains list of 2 wheelers brands
      TwoWheelerBrands: [],

      //data2 contains list of 4 wheelers brands
      FourWheelerBrands: [],
      data1: [],
      data2: []
    };
  }
  componentDidMount = async() => {
    try{
        await fetch("http://yoyorooms.in/fisso_gargage/appapi/getvechilcebrands.php")
            .then(response => response.json())
            .then(response2 => {
            //console.log(response2)
              
                if(response2.status==1){
                  this.setState({TwoWheelerBrands:response2.twoWheeler,FourWheelerBrands:response2.fourWheeler})
                
                }
                else{
                  // this.setState({loading:false})
                  // Toast.show({
                  //   text: "Something Went Wrong!",
                  //   textStyle: { textAlign: "center", color: '#111' },
                  //   duration: 2000,
                  //   position: 'top',
                  //   style: { borderRadius: 10, margin: responsiveWidth(10), backgroundColor: 'rgba(255,255,255,0.8)' }
                  // });
                }
            }).catch((error) =>{
                  //this.setState({loader:false})
                  console.log(error)
            });
        
    }
    catch(error){
          this.setState({loader:false})
          console.log(error)
    }
    let Temp1 = this.state.TwoWheelerBrands
    let Temp2 = this.state.FourWheelerBrands
    this.setState({ data2: Temp2, data1: Temp1 })

    Alert.alert(
      " Message From FISSO",
      "Hello User If you don't have services avaliable for 2 wheeler or 4 wheeler, then don't select the Vehicle type which you don't have. It will help users to find your garage fast",
      [{ text: "OK" }],
      { cancelable: false }
    );
  }


  //************************************* */
  onCheckChange4Wheels(id) {
    const data2 = this.state.data2;
    const index = data2.findIndex(x => x.id === id);
    data2[index].checked = !data2[index].checked;
    this.setState(data2);
  }
  editVehicles4wheels() {
    return this.state.data2.map((item, key) => {
      return (<View key={key} style={{ flexDirection: 'row', marginTop: 5 }}>
        <CheckBox checked={item.checked} onPress={() => this.onCheckChange4Wheels(item.id)} />
        <Text style={{ marginLeft: 15 }}>{item.key}</Text>
      </View>)
    })
  }
  //******************************************/
  onCheckChange2Wheels(id) {
    const data1 = this.state.data1;

    const index = data1.findIndex(x => x.id === id);
    data1[index].checked = !data1[index].checked;
    this.setState(data1);
  }

  editVehicles2wheels() {
    return this.state.data1.map((item, key) => {
      return (<View key={key} style={{ flexDirection: 'row', marginTop: 5 }}>
        <CheckBox checked={item.checked} onPress={() => this.onCheckChange2Wheels(item.id)} />
        <Text style={{ marginLeft: 15 }}>{item.key}</Text>
      </View>)
    })
  }

  renderEditVehicleScreens() {
    if (this.state.radio1 == true && this.state.radio2 == false) {
      return (
        <View style={{ height: responsiveHeight(53), width: responsiveWidth(80) }}>
          <ScrollView>
            {this.editVehicles4wheels()}
          </ScrollView>
        </View>
      )
    }
    else if (this.state.radio2 == true && this.state.radio1 == false) {
      return (
        <View style={{ height: responsiveHeight(53), width: responsiveWidth(80) }}>
          <ScrollView>
            {this.editVehicles2wheels()}
          </ScrollView>
        </View>
      )
    }
    else {
      return (
        <View style={{ height: responsiveHeight(53), alignItems: 'center', justifyContent: 'center' }}>
          <Text style={Styles.boldBlackText}>Select Vehicle Type</Text>
        </View>
      )
    }
  }

  saveDetails = async () => {
    var res1 = this.state.data1.map((t) => t.key)
    var res2 = this.state.data1.map((t) => t.checked)
    
    var resultArray = []
    for (let i = 0; i < res2.length; i++) {
      if (res2[i] == true) {
        resultArray.push(res1[i])
      }
    }
    var res3 = this.state.data2.map((t) => t.key)
    var res4 = this.state.data2.map((t) => t.checked)
    var resultArray1 = []
    for (let i = 0; i < res4.length; i++) {
      if (res4[i] == true) {
        resultArray1.push(res3[i])
      }
    }
    await this.setState({ Selected2wheelers: resultArray ,Selected4wheelers: resultArray1 })
    await this.state.garageDetails.push({
      vechileSelected: {
            twoWheelers:this.state.Selected2wheelers,
            fourWheeler:this.state.Selected4wheelers
      }, 
    })
    this.props.navigation.navigate("GarageServices", { garageDetails: this.state.garageDetails })
    //console.log( this.state.garageDetails)
  }

  render() {
    //  var details = this.props.navigation.getParam('garageDetails', 'NO-garage-Selected');
    // console.log(details)
    return (
      <Container>
        <ImageBackground source={require('../../../Components/Images/BG/Back.jpg')}
          style={{ height: height, width: width, alignItems: 'center', justifyContent: 'center', }}>
          <Card style={Styles.GarageSetupCustomCard}>
            <View style={{ flex: 0.15, backgroundColor: '#229CE3', borderTopLeftRadius: 10, borderTopRightRadius: 10, paddingHorizontal: responsiveWidth(2), justifyContent: 'center' }}>
              <Text style={Styles.HeaderText}>Garage Vehicles</Text>
              <Text style={{ color: '#fff', fontSize: responsiveFontSize(2) }}>Select Vehicles that can be serviced in your garage</Text>
            </View>
            <View style={{ flexDirection: 'row', margin: responsiveWidth(1), }}>
              <Text style={Styles.BoldBlueText}>Vehicles Type :  </Text>
              <View style={{ marginLeft: responsiveWidth(2), flexDirection: 'row' }}>

                <TouchableOpacity style={{ flexDirection: 'row', alignItems: 'center' }}
                  onPress={() => { this.setState({ radio2: true, radio1: false }) }}>
                  <Radio selected={this.state.radio2}
                    color={"#229CE3"}
                    selectedColor={'#229CE3'}
                    onPress={() => { this.setState({ radio2: true, radio1: false }) }} />
                  <Text style={{ marginLeft: responsiveWidth(1) }}>2 Wheeler</Text>
                </TouchableOpacity>

                <TouchableOpacity style={{ flexDirection: 'row', marginLeft: responsiveWidth(6), alignItems: 'center', }}
                  onPress={() => { this.setState({ radio1: true, radio2: false }) }}>
                  <Radio selected={this.state.radio1}
                    color={"#229CE3"}
                    selectedColor={'#229CE3'}
                    onPress={() => { this.setState({ radio1: true, radio2: false }) }} />
                  <Text style={{ marginLeft: responsiveWidth(1) }}>4 Wheeler</Text>
                </TouchableOpacity>
              </View>
            </View>

            <View style={{ flex: 0.77 }}>
              <View style={{ height: responsiveHeight(53), alignItems: 'center' }}>
                {this.renderEditVehicleScreens()}
              </View>

            </View>
            <View style={{ flex: 0.09, alignItems: 'center', justifyContent: 'center' }}>
              <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', }}>
                <TouchableOpacity style={Styles.BlueBorderButton}
                  onPressIn={() => {
                    this.props.navigation.goBack()
                  }}
                  onPressOut={() => { console.log("2 wheeler : " + this.state.Selected2wheelers + " 4 wheeler : " + this.state.Selected4wheelers); }}>
                  <Text style={Styles.BoldBlueText}>Back</Text>
                </TouchableOpacity>
                <TouchableOpacity style={Styles.normalButton}
                  onPress={() => { this.saveDetails() }}
                  //onPressOut={() => { this.props.navigation.navigate("GarageServices") }}
                  >
                  <Text style={Styles.BoldWhiteText}>Next</Text>
                </TouchableOpacity>
              </View>
            </View>
          </Card>
        </ImageBackground>
      </Container>
    );
  }
}

export default GarageVehicles;
