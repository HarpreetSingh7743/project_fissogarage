import React, { Component } from 'react';
import { View, Text, Image, StyleSheet, } from 'react-native';
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions';
import Display from 'react-native-display';
import * as Progress from 'react-native-progress';
import { SafeAreaView } from 'react-native-safe-area-context';
import Localstorage from "../../../async";

console.disableYellowBox = true;
class Splash extends Component {
  constructor(props) {
    super(props);
    this.state = {
      enable: false,
      enablelogo: false,
      enabletext: false,
      enableprogress: false,
      progressValue: 0
    };
  }
  componentDidMount = () => {
    this.setState({ enable: true, enabletext: true })
    setTimeout(() => {
      this.setState({ enablelogo: true, enable: false, enableprogress: true })
      setTimeout(() => {
        Localstorage.retrieveItem("uid").then((response) => {
          // console.log(response)
          if (!response) {
            Localstorage.retrieveItem("DevId").then((response) => {
              if (!response) {
                //alert("walk pe")
                this.props.navigation.navigate("walk");
              }
              else {
                //alert("auth")
                this.props.navigation.navigate("auth");
              }
            }).catch((error) => {
              console.log(error)
            });
          }
          else {
            //alert("main pe")
            Localstorage.retrieveItem("uStatus").then((response) => {
              if (response == 0) {
                //alert("walk pe")
                this.props.navigation.navigate("mainGarage");
              }
              else {
                //alert("auth")
                this.props.navigation.navigate("main");
              }
            }).catch((error) => {
              console.log(error)
            });
          }
        }).catch((error) => {
          console.log(error)
        });

      }, 3000);
    }, 2000);
  }

  render() {
    setTimeout((function () {
      this.setState({ progressValue: this.state.progressValue + (0.1 * Math.random()) });
    }).bind(this), 1000);
    return (
      <View style={styles.container}>
        <View style={{ flex: 1, alignItems: "center", justifyContent: "flex-end" }}>

          <Display enable={this.state.enabletext}
            enterDuration={2000}
            enter="zoomIn" >
            <Image source={require("../../Components/Images/Logo/LogoFull.png")}
              style={{ width: responsiveWidth(40), height: responsiveHeight(10) }} />
          </Display>
        </View>
        <View style={{ flex: 1, alignItems: "center", justifyContent: "flex-end" }}>
          <Display enable={this.state.enableprogress}
            enterDuration={2000}
            enter="fadeIn" >
            <Progress.Bar progress={this.state.progressValue} width={responsiveWidth(30)} color="#fff"
              style={{ marginBottom: responsiveHeight(5) }} />
          </Display>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: "#229CE3"

  },
  loader: {
    position: "absolute",
    bottom: 100,
    alignSelf: "center",
  }

})

export default Splash;