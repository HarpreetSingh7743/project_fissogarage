import React, { Component } from 'react';
import { View, Text, SafeAreaView, TouchableOpacity, TextInput, Modal, Image } from 'react-native';
import { Container, Card } from 'native-base';
import { responsiveHeight, responsiveWidth } from 'react-native-responsive-dimensions';
import Styles from '../Styles/Styles';
import MapView, { Marker } from 'react-native-maps';

class OpenMapModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            latitude: this.props.lat,
            longitude: this.props.lng,
            modalVisible: true
        };
    }

    render() {
        return (
            <Modal visible={this.state.modalVisible} transparent={true} onRequestClose={() => this.props.onAction(false)}>
                <SafeAreaView style={{ backgroundColor: 'rgba(1,1,1,0.8)', flex: 1, alignItems: 'center', justifyContent: 'center' }}>

                    <Card style={{ height: responsiveHeight(80), width: responsiveWidth(95) }}>
                        <View style={{ flex: 0.05, alignItems: 'center', justifyContent: 'center' }}>
                            <Text style={{ color: '#f11' }}>Press and hold the Marker to move</Text>
                        </View>
                        <View style={{ flex: 0.85, alignItems: 'center', justifyContent: 'center' }}>
                            <MapView style={{ height: responsiveHeight(69), width: responsiveWidth(93) }}
                                initialRegion={{
                                    latitude:parseFloat(this.state.latitude),
                                    longitude: parseFloat(this.state.longitude),
                                    latitudeDelta: 0.05,
                                    longitudeDelta: 0.05,
                                }}
                                showsUserLocation={true}
                                showsMyLocationButton={true}>
                                <Marker draggable
                                    title="Drop it at your Garage Location"
                                    coordinate={{ latitude:parseFloat(this.state.latitude), longitude: parseFloat(this.state.longitude) }}
                                    onDragEnd={(e) => this.setState({ latitude: e.nativeEvent.coordinate.latitude, longitude: e.nativeEvent.coordinate.longitude })}>
                                    <Image source={require('../Images/Icons/MapMarker.png')} style={{width:responsiveWidth(8),height:responsiveHeight(5.5)}}/>
                                </Marker>
                            </MapView>

                        </View>
                        <View style={{ flex: 0.1, flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                            <TouchableOpacity onPress={() => { this.props.onAction(false) }} style={Styles.BlueBorderButton}>
                                <Text style={Styles.BoldBlueText}>Cancel</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={Styles.normalButton} onPress={() => this.props.onConfirm(false, this.state.latitude, this.state.longitude)}>
                                <Text style={Styles.BoldWhiteText}>Confirm</Text>
                            </TouchableOpacity>
                        </View>
                    </Card>
                </SafeAreaView>
            </Modal>
        );
    }
}

export default OpenMapModal;
