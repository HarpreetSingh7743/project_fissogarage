import React, { Component } from 'react';
import { View, Text, Modal, SafeAreaView, TouchableOpacity, ScrollView, TextInput } from 'react-native';
import { responsiveWidth, responsiveHeight } from 'react-native-responsive-dimensions';
import Styles from '../Styles/Styles';
import { Icon } from 'native-base';

class EditServicesModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            visibleModal: true,
            AddedService: '',
            cloneCheck: true,
            AddedServicePrice: '',
            RequestedServices: this.props.inner,
            TempArray: [],
            TotalBill: this.props.TotalBill
        }
    }
    componentDidMount() {
        let Temp = this.state.RequestedServices
        this.setState({ TempArray: Temp })
    }
    showServices() {
        return this.state.TempArray.map((t) => {
            return (
                <View style={{ width: responsiveWidth(95), flexDirection: 'row', marginTop: responsiveWidth(2), justifyContent: 'center', alignItems: 'center', }} key={t.ServiceName}>
                    <View style={{ flex: 0.6 }}>
                        <Text>{t.ServiceName}</Text>
                    </View>
                    <View style={{ flex: 0.3 }}>
                        <Text>Rs. {t.Price}</Text>
                    </View>
                    <View style={{ flex: 0.1 }}>
                        <TouchableOpacity onPress={() => { this.removeService(t) }}>
                            <Icon name="circle-with-cross" type="Entypo" style={{ color: '#f11' }} />
                        </TouchableOpacity>
                    </View>
                </View>
            )
        })

    }

    addService() {
        let { AddedService, AddedServicePrice, TempArray } = this.state
        if (AddedService != '' && AddedServicePrice != '') {
            let obj = {}
            obj = { ServiceName: AddedService, Price: parseInt(AddedServicePrice) }
            TempArray.push(obj)
            this.setState({ TempArray: TempArray, AddedService: '', AddedServicePrice: '' })
        }
        else {
            alert("Enter Service Details")
        }
        this.totalBill()
    }

    removeService = (t) => {
        var arr = this.state.TempArray
        var pos = arr.indexOf(t);
        arr.splice(pos, 1)
        this.setState({ TempArray: arr })

        this.totalBill()
    }

    totalBill() {
        let TempArr = this.state.TempArray
        let Temp = [], total = 0
        for (let i = 0; i < TempArr.length; i++) {
            Temp.push(TempArr[i].Price)
        }
        for (let i = 0; i < Temp.length; i++) {
            total = total + Temp[i]
        }
        this.setState({ TotalBill: total })
    }

    render() {

        return (
            <Modal visible={this.state.visibleModal} transparent={true} onRequestClose={() => { this.props.onAction(false) }}>
                <SafeAreaView style={{ flex: 1, backgroundColor: 'rgba(1,1,1,0.3)', alignItems: 'center', justifyContent: 'center' }}>
                    <View style={{ width: responsiveWidth(95), height: responsiveHeight(70), backgroundColor: "#fff", borderRadius: 10 }}>
                        <View style={{ flex: 0.05, backgroundColor: '#229CE3', borderTopRightRadius: 10, borderTopLeftRadius: 10, padding: responsiveWidth(2), flexDirection: 'row' }}>
                            <View style={{ flex: 0.8 }}>
                                <Text style={Styles.BoldWhiteText}>Requested Services</Text>
                            </View>
                            <View style={{ flex: 0.2, alignItems: 'flex-end' }}>
                                <TouchableOpacity onPress={() => { this.props.onAction(false) }}>
                                    <Icon name="md-close" type="Ionicons" style={{ color: '#fff' }} />
                                </TouchableOpacity>
                            </View>
                        </View>
                        <View style={{ flex: 0.07, backgroundColor: '#229CE3', flexDirection: 'row', justifyContent: 'center', }}>
                            <TextInput style={{ width: responsiveWidth(50), borderBottomWidth: 1, borderColor: '#fff', marginBottom: 8, color: '#fff' }}
                                placeholder="Enter Service Name"
                                placeholderTextColor="#fff"
                                onSubmitEditing={() => { this.refs.price.focus() }}
                                value={this.state.AddedService}
                                onChangeText={(AddedService) => { this.setState({ AddedService: AddedService }) }} />

                            <TextInput style={{ width: responsiveWidth(20), borderBottomWidth: 1, marginLeft: responsiveWidth(3), borderColor: '#fff', marginBottom: 8, color: '#fff' }}
                                placeholder="Enter Price"
                                placeholderTextColor="#fff"
                                ref={'price'}
                                keyboardType="number-pad"
                                onSubmitEditing={() => { this.addService() }}
                                value={this.state.AddedServicePrice}
                                onChangeText={(AddedServicePrice) => { this.setState({ AddedServicePrice: AddedServicePrice }) }} />

                            <TouchableOpacity onPress={() => { this.addService() }}>
                                <Icon name="md-add" type="Ionicons" style={{ color: '#fff', marginLeft: responsiveWidth(3) }} />
                            </TouchableOpacity>
                        </View>
                        <View style={{ flex: 0.83, padding: responsiveWidth(1), alignItems: "center" }}>
                            <ScrollView>
                                {this.showServices()}
                            </ScrollView>
                        </View>
                        <View style={{ flex: 0.1, alignItems: 'center', flexDirection: 'row', justifyContent: 'center' }}>
                            <View style={{ flex: 0.5, paddingLeft: responsiveWidth(2) }}>
                                <Text>Total Bill: ₹ {this.state.TotalBill}</Text>
                            </View>
                            <View style={{ flex: 0.5, alignItems: 'center', justifyContent: 'center', }}>
                                <TouchableOpacity style={Styles.normalButton}
                                    onPress={() => { this.setState({ RequestedServices: this.state.TempArray }), this.props.onAction(false) }}>
                                    <Text style={Styles.BoldWhiteText}>Confirm</Text>
                                </TouchableOpacity>
                            </View>
                        </View>

                    </View>
                </SafeAreaView>
            </Modal>
        );
    }
}

export default EditServicesModal;
