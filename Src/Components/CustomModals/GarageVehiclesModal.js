import React, { Component } from 'react';
import { View, Text, SafeAreaView, TouchableOpacity, ScrollView, Modal } from 'react-native';
import { responsiveWidth, responsiveHeight } from 'react-native-responsive-dimensions';
import Styles from '../Styles/Styles';
import { Icon, Radio, CheckBox } from "native-base";


class GarageVehiclesModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            visibleModal: true,
            two_wheelers: this.props.two_wheelers,
            four_wheelers: this.props.four_wheelers,
            showVehicles4wheels: [],
            showVehicles2wheels: [],
            radio1: false,
            radio2: true,
            vechicleTypes: 'none',
            buttonText: 'Edit',
            data1: [
                { id: 1, key: "Hero", checked: false },
                { id: 2, key: "Bajaj", checked: false },
                { id: 3, key: "KTM", checked: false },
                { id: 4, key: "Honda", checked: false },
                { id: 5, key: "Royal Enfield", checked: false },
                { id: 6, key: "TVS", checked: false },
                { id: 7, key: "Yahama", checked: false },
                { id: 8, key: "Suzuki", checked: false },
                { id: 9, key: "Piaggio", checked: false },
                { id: 10, key: "Kawasaki", checked: false },
                { id: 11, key: "UM Motorcycles", checked: false },
            ],
            data2: [
                { id: 1, key: "Honda", checked: false },
                { id: 2, key: "Maruti", checked: false },
                { id: 3, key: "BMW", checked: false },
                { id: 4, key: "Tata", checked: false },
                { id: 5, key: "Tesla", checked: false },
                { id: 6, key: "Toyota", checked: false },
                { id: 7, key: "bentlay", checked: false },
                { id: 8, key: "Hyundai", checked: false },
                { id: 9, key: "Nissan", checked: false },
                { id: 10, key: "Chevrolet", checked: false },
                { id: 11, key: "volkswagen", checked: false },
                { id: 12, key: "Volvo", checked: false },
                { id: 13, key: "Jeep", checked: false },
                { id: 14, key: "Fiat", checked: false },
                { id: 15, key: "Ford", checked: false },

            ]
        };
    }
    componentDidMount() {
        
    }


    renderEditVehicleScreens() {
        if (this.state.radio1 == true && this.state.radio2 == false) {
            return (
                <View style={{ height: responsiveHeight(53), width: responsiveWidth(80) }}>
                    <ScrollView>
                        {this.editVehicles4wheels()}
                    </ScrollView>
                </View>
            )
        }
        else if (this.state.radio1 == false && this.state.radio2 == true) {
            return (
                <View style={{ height: responsiveHeight(53), width: responsiveWidth(80) }}>
                    <ScrollView>
                        {this.editVehicles2wheels()}
                    </ScrollView>
                </View>
            )
        }
    }
    //************************************* */
    onCheckChange4Wheels(id) {
        const data2 = this.state.data2;

        const index = data2.findIndex(x => x.id === id);
        data2[index].checked = !data2[index].checked;
        this.setState(data2);
    }
    editVehicles4wheels() {
        return this.state.data2.map((item, key) => {
            return (<View key={key} style={{ flexDirection: 'row', marginTop: 5 }}>
                <CheckBox checked={item.checked} onPress={() => this.onCheckChange4Wheels(item.id)} />
                <Text style={{ marginLeft: 15 }}>{item.key}</Text>
            </View>)
        })
    }
    //******************************************/
    onCheckChange2Wheels(id) {
        const data1 = this.state.data1;

        const index = data1.findIndex(x => x.id === id);
        data1[index].checked = !data1[index].checked;
        this.setState(data1);
    }

    editVehicles2wheels() {
        return this.state.data1.map((item, key) => {
            return (<View key={key} style={{ flexDirection: 'row', marginTop: 5 }}>
                <CheckBox checked={item.checked} onPress={() => this.onCheckChange2Wheels(item.id)} />
                <Text style={{ marginLeft: 15 }}>{item.key}</Text>
            </View>)
        })
    }

    saveDetails() {
        var res1 = this.state.data1.map((t) => t.key)
        var res2 = this.state.data1.map((t) => t.checked)
        var resultArray = []
        for (let i = 0; i < res2.length; i++) {
            if (res2[i] == true) {
                resultArray.push(res1[i])
            }
        }

        var res3 = this.state.data2.map((t) => t.key)
        var res4 = this.state.data2.map((t) => t.checked)
        var resultArray1 = []
        for (let i = 0; i < res4.length; i++) {
            if (res4[i] == true) {
                resultArray1.push(res3[i])
            }
        }
        this.setState({ showVehicles4wheels: resultArray1 })
        this.setState({ showVehicles2wheels: resultArray, buttonText: "Edit" })
        console.log(this.state.showVehicles2wheels);

    }
    render() {
        return (
            <Modal visible={this.state.visibleModal} transparent={true} onRequestClose={() => { this.props.onExit(false) }}>
                <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', backgroundColor: 'rgba(1,1,1,0.7)' }}>
                    <View style={{ flex:0.7, backgroundColor: '#fff', borderRadius: 15 }}>
                        <View style={{ flex:0.1,paddingLeft: responsiveWidth(2), backgroundColor: '#229CE3', borderTopLeftRadius: 15, borderTopRightRadius: 15 }}>
                            <Text style={Styles.HeaderText}>Vehicles</Text>

                        </View>
                        <View style={{flex:0.05, flexDirection: 'row', margin: responsiveWidth(2) }}>
                            <Text style={Styles.BoldBlueText}>Vehicles Type :  </Text>
                            <View style={{ marginLeft: responsiveWidth(2), flexDirection: 'row' }}>


                                <TouchableOpacity style={{ flexDirection: 'row' }}
                                    onPress={() => { this.setState({ radio2: true, radio1: false }) }}>
                                    <Radio selected={this.state.radio2}
                                        color={"#229CE3"}
                                        selectedColor={'#229CE3'}
                                        onPress={() => { this.setState({ radio2: true, radio1: false }) }} />
                                    <Text style={{ marginLeft: responsiveWidth(1) }}>2 Wheeler</Text>
                                </TouchableOpacity>

                                <TouchableOpacity style={{ flexDirection: 'row', marginLeft: responsiveWidth(6) }}
                                    onPress={() => { this.setState({ radio1: true, radio2: false, vechicleTypes: '4Wheel' }) }}>
                                    <Radio selected={this.state.radio1}
                                        color={"#229CE3"}
                                        selectedColor={'#229CE3'}
                                        onPress={() => { this.setState({ radio1: true, radio2: false, vechicleTypes: '4Wheel' }) }} />
                                    <Text style={{ marginLeft: responsiveWidth(1) }}>4 Wheeler</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                        <View style={{ flex:0.75, alignItems: 'center' }}>
                            {this.renderEditVehicleScreens()}
                        </View>
                        <View style={{flex: 0.1, flexDirection: 'row', width: responsiveWidth(90), alignItems: 'center', justifyContent: 'center', }}>
                            <View style={{ flex: 0.5 }}>
                                <TouchableOpacity style={Styles.normalButton} onPress={() => { this.props.onExit(false) }}>
                                    <Text style={Styles.BoldWhiteText}>Cancel</Text>
                                </TouchableOpacity>
                            </View>
                            <View style={{ flex: 0.5 }}>
                                <TouchableOpacity style={Styles.normalButton} onPress={() => { this.saveDetails() }}>
                                    <Text style={Styles.BoldWhiteText}>Confirm Changes</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </View>
            </Modal>
        );
    }
}

export default GarageVehiclesModal;
