import React, { Component } from 'react';
import { View, Text, Modal, TouchableOpacity } from 'react-native';
import { Card, Icon,Spinner,Toast } from 'native-base';
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions';
import { TextInput } from 'react-native-gesture-handler';
import Styles from '../Styles/Styles';

class EnterOTPModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            UserEmail: this.props.navigation.getParam('u_email', 'NO-email-Selected'),
            visibleModal: true,
            one: "",
            two: "",
            three: "",
            four: "",
            otp: this.props.navigation.getParam('u_otp', 'NO-otp-Selected'),
            loading:false
        };
    }

    handleClick = () => {
        let OTP = parseInt(this.state.one + this.state.two + this.state.three + this.state.four)
        this.setState({loading:true})
        if (this.state.otp == OTP) 
        {   
            try{
                fetch("http://yoyorooms.in/fisso_gargage/appapi/verify_email.php", 
                    {
                        method: 'POST',
                        headers: {
                            Accept: 'application/json',
                            'Content-Type': 'application/json',
                        },
                        body: JSON.stringify({
                                  u_email :this.state.UserEmail,
                                }),     
                    })
                    .then(response => response.json())
                    .then(response2 => {
                    console.log(response2)
                      
                        if(response2==1){
                          this.setState({loading:false})
                          this.props.navigation.navigate("mainGarage")
                         
                        }
                        else{
                          this.setState({loading:false})
                          Toast.show({
                            text: "Something Went Wrong!",
                            textStyle: { textAlign: "center", color: '#111' },
                            duration: 2000,
                            position: 'top',
                            style: { borderRadius: 10, margin: responsiveWidth(10), backgroundColor: 'rgba(255,255,255,0.8)' }
                          });
                        }
                    }).catch((error) =>{
                          this.setState({loading:false})
                          console.log(error)
                    });
                
            }
            catch(error){
                  this.setState({loading:false})
                  console.log(error)
            }
        }
        else 
        {   
            this.setState({loading:false})
            Toast.show({
                  text: "You enetered a wrong OTP!",
                  textStyle: { textAlign: "center", color: '#111' },
                  duration: 2000,
                  position: 'top',
                  style: { borderRadius: 10, margin: responsiveWidth(10), backgroundColor: 'rgba(255,255,255,0.8)' }
                }); 
        }
    }

    render() {
    

        return (
                <View style={{ flex: 1, backgroundColor: '#229CE3', alignItems: 'center', justifyContent: 'center', }}>
                    <Card style={{ width: responsiveWidth(80), height: "auto", borderRadius: 15, padding: responsiveWidth(3) }}>
                        <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', }}>
                            <View style={{ flex: 0.8 }}>
                                <Text style={{ color: '#229CE3', fontSize: responsiveFontSize(3), fontWeight: 'bold' }}>
                                    Enter OTP</Text>
                            </View>
                            {/* <View style={{ flex: 0.2, alignItems: 'flex-end' }}>
                                <TouchableOpacity onPress={() => { this.props.navigation.goBack() }}>
                                    <Icon name="close" type="MaterialIcons" />
                                </TouchableOpacity>
                            </View> */}
                        </View>
                        <Text style={{ color: '#888' }}>Enter the OTP sent to <Text style={{ color: '#229CE3' }}>{this.state.UserEmail}</Text></Text>
                        <View style={{ marginVertical: responsiveWidth(7), flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                            <View style={styles.OtpBox}>
                                <TextInput style={styles.OtpInput}
                                    keyboardType="number-pad"
                                    onChangeText={(one) => { this.setState({ one: one }), this.refs.two.focus() }} />
                            </View>

                            <View style={styles.OtpBox}>
                                <TextInput style={styles.OtpInput}
                                    keyboardType="number-pad"
                                    onChangeText={(two) => { this.setState({ two: two }), this.refs.three.focus() }}
                                    ref={'two'} />
                            </View>

                            <View style={styles.OtpBox}>
                                <TextInput style={styles.OtpInput}
                                    keyboardType="number-pad"
                                    onChangeText={(three) => { this.setState({ three: three }), this.refs.four.focus() }}
                                    ref={'three'} />
                            </View>

                            <View style={styles.OtpBox}>
                                <TextInput style={styles.OtpInput}
                                    keyboardType="number-pad"
                                    onChangeText={(four) => { this.setState({ four: four }) }}
                                    ref={'four'} />
                            </View>
                        </View>

                        <View style={{ width: responsiveWidth(75), alignItems: 'center', }}>
                            {!this.state.loading&&
                                <TouchableOpacity style={Styles.BlueBorderButton} onPress={() => {
                                    this.handleClick()
                                }}>
                                    <Text style={Styles.BoldBlueText}>verify</Text>
                                </TouchableOpacity>
                            }
                            {this.state.loading &&
                                <TouchableOpacity style={Styles.BlueBorderButton} >
                                    <Spinner size="small" color = "#229CE3"/>
                                </TouchableOpacity>
                            }
                        </View>
                    </Card>
                </View>
        );
    }
}

export default EnterOTPModal;

const styles = {
    OtpBox: {
        borderWidth: 1,
        height: responsiveHeight(5),
        width: responsiveWidth(10),
        marginHorizontal: responsiveWidth(2.5),
        borderRadius: 10,
        alignItems: 'center',
        justifyContent: 'center',
    },
    OtpInput: {
        width: responsiveWidth(4),
        fontSize: responsiveFontSize(2.5)
    }
}