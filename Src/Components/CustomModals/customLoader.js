import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, Image,Modal} from 'react-native';
import { responsiveWidth, responsiveHeight } from 'react-native-responsive-dimensions';

export default class PreLoader extends Component {
  _renderLoader = () => {
    if (this.props.isVisableLoader) return (
        <Modal hardwareAccelerated={true}>
            <View style={{flex:1,alignItems:"center",justifyContent:"center"}}>
                <View style={{alignItems:"center",justifyContent:"center",padding:responsiveWidth(10),backgroundColor:"#229CE3",borderRadius:responsiveWidth(5)}}>
                    <Image source={require('../../Components/Images/Loader/loader.gif')}  resizeMode="stretch" 
                            style={{height:responsiveWidth(12),width:responsiveWidth(12)}}/>
                </View>
                
            </View>
        </Modal>
    )
    else return null;
  }
  render () {
    return (
      this._renderLoader()
    )
  }
}
const styles = StyleSheet.create ({
    background: {
      backgroundColor: "#229CE3",
      flex: 1,
      position: 'absolute',
      top: 0,
      bottom: 0,
      left: 0,
      right: 0,
      alignItems: 'center',
      justifyContent: 'center'
    }
  });