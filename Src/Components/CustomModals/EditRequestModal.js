import React, { Component } from 'react';
import { View, Text, Modal, SafeAreaView, TouchableOpacity, ScrollView, TextInput } from 'react-native';
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions';
import Styles from '../Styles/Styles';
import { Icon } from 'native-base';

class EditRequestModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            visibleModal: true,
            RequestedServices: [],
            TempArray: this.props.inner,
            TotalBill: 0,
            changedtext: 0
        }
    }
    setPrice = (v, i) => {
        let Temp = this.state.TempArray
        console.log(i)
        if (i < Temp.length-1) {


            this.refs[i + 1].focus();
            let servName = Temp[i].ServiceName;
            console.log(i);
            console.log(servName)
            Temp.splice(i, 1, { ServiceName: servName, Price: v.nativeEvent.text })
            this.setState({ TempArray: Temp })
            this.totalBill();
            //console.log(this.state.TempArray)

        }
        else {
            let servName = Temp[i].ServiceName;
            console.log(i);
            console.log(servName)
            Temp.splice(i, 1, { ServiceName: servName, Price: v.nativeEvent.text })
            this.setState({ TempArray: Temp })
            this.totalBill();
            alert("Done")
        }
    }


    totalBill = () => {
        let TempArr = this.state.TempArray;
        this.setState({ TotalBill: tprice })
        var tprice = 0;
        for (let i = 0; i < TempArr.length; i++) {
            tprice = parseInt(tprice) + parseInt(TempArr[i].Price);
        }
        console.log(tprice);

        this.setState({ TotalBill: tprice })
        //tprice=0

    }

    render() {
        return (
            <Modal visible={this.state.visibleModal} transparent={true} onRequestClose={() => { this.props.onAction(false) }}>
                <View style={{ flex: 1, backgroundColor: 'rgba(1,1,1,0.3)', alignItems: 'center', justifyContent: 'center' }}>
                    <View style={{ width: responsiveWidth(95), height: responsiveHeight(70), backgroundColor: "#fff", borderRadius: 10 }}>
                        <View style={{ flex: 0.05, backgroundColor: '#229CE3', borderTopRightRadius: 10, borderTopLeftRadius: 10, padding: responsiveWidth(2), flexDirection: 'row' }}>
                            <View style={{ flex: 0.8 }}>
                                <Text style={Styles.BoldWhiteText}>Requested Services</Text>
                            </View>
                            <View style={{ flex: 0.2, alignItems: 'flex-end' }}>
                                <TouchableOpacity onPress={() => { this.props.onAction(false) }}>
                                    <Icon name="md-close" type="Ionicons" style={{ color: '#fff' }} />
                                </TouchableOpacity>
                            </View>
                        </View>
                        <View style={{ flex: 0.05, justifyContent: 'center', backgroundColor: "#229CE3" }}>
                            <Text style={{ color: '#fff', marginLeft: responsiveWidth(2) }}>Enter The Price of Requested Service</Text>
                        </View>

                        <View style={{ flex: 1, padding: responsiveWidth(1), alignItems: "center" }}>
                            {(this.state.TempArray.length > 0) &&
                                this.state.TempArray.map((t, i) => (
                                    <View key={i} style={{ width: responsiveWidth(95), flexDirection: 'row', marginTop: responsiveWidth(2), justifyContent: 'center', alignItems: 'center', }} key={t.ServiceName}>
                                        <View style={{ flex: 0.6 }}>
                                            <Text>{t.ServiceName}</Text>
                                        </View>
                                        <View style={{ flex: 0.3 }}>
                                            <TextInput style={{ width: responsiveWidth(25), borderColor: '#229CE3', borderBottomWidth: 1 }}
                                                placeholder="Enter Price"
                                                //value={t.Price}
                                                ref={i}
                                                keyboardType='numeric'
                                                keyboardType='numeric'
                                                returnKeyType="next"
                                                blurOnSubmit={false}
                                                autoCorrect={false}
                                                onSubmitEditing={(val) => { this.setPrice(val, i) }}
                                            />
                                        </View>
                                    </View>
                                ))}
                        </View>
                        <View style={{ flex: 0.1, alignItems: 'center', flexDirection: 'row', justifyContent: 'center' }}>
                            <View style={{ flex: 0.5, paddingLeft: responsiveWidth(2) }}>
                                <Text>Total Bill: ₹ {this.state.TotalBill}</Text>
                            </View>
                            <View style={{ flex: 0.5, alignItems: 'center', justifyContent: 'center', }}>
                                <TouchableOpacity style={Styles.normalButton}
                                    onPress={() => {
                                        //      this.setState({ RequestedServices: this.state.TempArray }),
                                        //  this.props.onAction(false)
                                        console.log(this.state.TempArray);

                                    }}>
                                    <Text style={Styles.BoldWhiteText}>Confirm</Text>
                                </TouchableOpacity>
                            </View>
                        </View>

                    </View>
                </View>
            </Modal>
        );
    }
}

export default EditRequestModal;
