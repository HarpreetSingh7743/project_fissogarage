import React, { Component } from 'react';
import { View, Text, Modal, SafeAreaView, TouchableOpacity, ScrollView } from 'react-native';
import { responsiveWidth, responsiveHeight } from 'react-native-responsive-dimensions';
import Styles from '../Styles/Styles';
import { Icon } from 'native-base';

class ShowServicesModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            visibleModal: true,
            RequestedServices: this.props.SelectedServices,
            TotalBill: 0
        };
    }

    componentDidMount(){
        let TempArr = this.state.RequestedServices
        let Temp = [], total = 0
        for (let i = 0; i < TempArr.length; i++) {
            Temp.push(TempArr[i].Price)
        }
        for (let i = 0; i < Temp.length; i++) {
            total = total + Temp[i]
        }
        this.setState({ TotalBill: total })
    }
    showServices() {

        return this.state.RequestedServices.map((t) => {
            return (
                <View style={{ width: responsiveWidth(90), flexDirection: 'row', marginTop: responsiveWidth(2), justifyContent: 'center', alignItems: 'center', }} key={t.ServiceName}>
                    <View style={{ flex: 0.7 }}>
                        <Text>{t.ServiceName}</Text>
                    </View>
                    <View style={{ flex: 0.3 }}>
                        <Text>₹ {t.Price}</Text>
                    </View>
                </View>
            )
        })
    }
    render() {
        return (
            <Modal visible={this.state.visibleModal} transparent={true} onRequestClose={() => { this.props.onAction(false) }}>
                <SafeAreaView style={{ flex: 1, backgroundColor: 'rgba(1,1,1,0.3)', alignItems: 'center', justifyContent: 'center' }}>
                    <View style={{ width: responsiveWidth(90), height: responsiveHeight(70), backgroundColor: "#fff", borderRadius: 10 }}>
                        <View style={{ flex: 0.05, backgroundColor: '#229CE3', borderTopRightRadius: 10, borderTopLeftRadius: 10, padding: responsiveWidth(2), flexDirection: 'row' }}>
                            <View style={{ flex: 0.8 }}>
                                <Text style={Styles.BoldWhiteText}>Requested Services</Text>
                            </View>
                            <View style={{ flex: 0.2, alignItems: 'flex-end' }}>
                                <TouchableOpacity onPress={() => { this.props.onAction(false) }}>
                                    <Icon name="md-close" type="Ionicons" style={{ color: '#fff' }} />
                                </TouchableOpacity>
                            </View>
                        </View>
                        <View style={{ flex: 0.9, padding: responsiveWidth(1) }}>
                            <ScrollView>
                                {this.showServices()}
                            </ScrollView>
                        </View>
                        <View style={{ flex: 0.05, flexDirection: 'row'}}>
                            <View style={{ flex: 0.7 }}></View>
                            <View style={{ flex: 0.3 }}>
                                <Text>Total : ₹{this.state.TotalBill}</Text>
                            </View>
                        </View>
                    </View>
                </SafeAreaView>
            </Modal>
        );
    }
}

export default ShowServicesModal;
