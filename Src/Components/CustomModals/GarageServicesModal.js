import React, { Component } from 'react';
import { View, Text, TouchableOpacity, ScrollView, TextInput, Modal } from 'react-native';
import { responsiveWidth, responsiveHeight } from 'react-native-responsive-dimensions';
import Styles from '../Styles/Styles';
import { Button, Icon, Radio } from 'native-base';

export default class GarageServicesModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            radio1: false,
            radio2: true,
            vechicleTypes: '',
            editservices4Wheels: this.props.four_Wheeler_services,
            editservices2Wheels: this.props.two_Wheeler_services,
            currentScreen: 'showServices',
            addservices4Wheels: '',
            addservices2Wheels: '',
            changesDone: false
        };
    }

    componentDidMount() {
       // console.log(this.state.editservices4Wheels);
    }
    renderScreen() {
        if (this.state.radio1 == true && this.state.radio2 == false) {
            return (
                <View style={{ height: responsiveHeight(53), width: responsiveWidth(87) }}>
                    <ScrollView>
                        {this.editservices4Wheels()}
                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <TextInput style={{
                                width: responsiveWidth(60), borderWidth: 1, marginHorizontal: responsiveWidth(2),
                                borderTopLeftRadius: 10, borderBottomLeftRadius: 10
                            }}
                                value={this.state.addservice}
                                onChangeText={(addservices4Wheels) => { this.setState({ addservices4Wheels: addservices4Wheels }) }}
                                returnKeyType="go"
                                onSubmitEditing={() => { this.addservices4Wheels() }} />
                            <TouchableOpacity style={{
                                backgroundColor: "#229CE3", paddingHorizontal: responsiveWidth(6),
                                paddingVertical: responsiveWidth(1.3), borderTopRightRadius: 10, borderBottomRightRadius: 10
                            }}
                                onPress={() => { this.addservices4Wheels() }}>
                                <Text style={Styles.BoldWhiteText}>Add</Text>
                            </TouchableOpacity>
                        </View>
                    </ScrollView>
                </View>
            )
        }
        else if (this.state.radio1 == false && this.state.radio2 == true) {
            return (
                <View style={{ height: responsiveHeight(53), width: responsiveWidth(87) }}>
                    <ScrollView>
                        {this.editservices2Wheels()}
                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <TextInput style={{
                                width: responsiveWidth(60), borderWidth: 1, marginHorizontal: responsiveWidth(2),
                                borderTopLeftRadius: 10, borderBottomLeftRadius: 10
                            }}
                                value={this.state.addservice}
                                onChangeText={(addservices2Wheels) => { this.setState({ addservices2Wheels: addservices2Wheels }) }}
                                returnKeyType="go"
                                onSubmitEditing={() => { this.addservices2Wheels() }} />
                            <TouchableOpacity style={{
                                backgroundColor: "#229CE3", paddingHorizontal: responsiveWidth(6),
                                paddingVertical: responsiveWidth(1.3), borderTopRightRadius: 10, borderBottomRightRadius: 10
                            }}
                                onPress={() => { this.addservices2Wheels() }}>
                                <Text style={Styles.BoldWhiteText}>Add</Text>
                            </TouchableOpacity>
                        </View>
                    </ScrollView>
                </View>
            )
        }
    }

    deleteservices4Wheels = (t) => {
        var arr = this.state.editservices4Wheels
        var pos = arr.indexOf(t);
        arr.splice(pos, 1)
        this.setState({ editservices4Wheels: arr })

        this.setState({ changesDone: true })
    }
    deleteservices2Wheels = (t) => {
        var arr = this.state.editservices2Wheels
        var pos = arr.indexOf(t);
        arr.splice(pos, 1)
        this.setState({ editservices2Wheels: arr })

        this.setState({ changesDone: true })
    }

    addservices4Wheels() {
        var arr = this.state.editservices4Wheels;
        if (this.state.addservices4Wheels == "") {
            alert("Write Service Name for 4 Wheeler")
        }
        else {
            arr.push({ GarageId: this.state.editservices4Wheels[0].GarageId, s_category: "4", s_id: "", s_name: this.state.addservices4Wheels });
            this.setState({ editservices4Wheels: arr, addservices4Wheels: '' })
            //console.log(this.state.editservices4Wheels);

        }

        this.setState({ changesDone: true })
    }
    addservices2Wheels() {
        var arr = this.state.editservices2Wheels;
        if (this.state.addservices2Wheels == "") {
            alert("Write Service Name for 2 Wheeler")
        }
        else {
            arr.push({ GarageId: this.state.editservices2Wheels[0].GarageId, s_category: "2", s_id: "", s_name: this.state.addservices2Wheels });
            this.setState({ editservices2Wheels: arr, addservices2Wheels: '' })
           // console.log(this.state.editservices2Wheels);
        }
        this.setState({ changesDone: true })
    }

    editservices4Wheels() {
        return this.state.editservices4Wheels.map(t => {
            return (
                <View key={t.s_id}>
                    <View style={{ marginBottom: responsiveWidth(2), flexDirection: 'row', alignItems: 'center' }}>
                        <View style={{ flex: 0.8 }}>
                            <Text style={Styles.boldBlackText}>{t.s_name}</Text>
                        </View>
                        <View style={{ flex: 0.2, alignItems: 'center' }}>
                            <TouchableOpacity onPress={() => { this.deleteservices4Wheels(t) }}>
                                <Icon name="circle-with-cross" type="Entypo" style={{ color: '#f11' }} />
                            </TouchableOpacity>
                        </View>

                    </View>
                </View>
            )
        })
    }

    editservices2Wheels() {
        return this.state.editservices2Wheels.map(t => {
            return (
                <View key={t.s_id}>
                    <View style={{ marginBottom: responsiveWidth(2), flexDirection: 'row', alignItems: 'center' }}>
                        <View style={{ flex: 0.8 }}>
                            <Text style={Styles.boldBlackText}>{t.s_name}</Text>
                        </View>
                        <View style={{ flex: 0.2, alignItems: 'center' }}>
                            <TouchableOpacity onPress={() => { this.deleteservices2Wheels(t) }}>
                                <Icon name="circle-with-cross" type="Entypo" style={{ color: '#f11' }} />
                            </TouchableOpacity>
                        </View>

                    </View>
                </View>
            )
        })
    }
    renderButton() {
        if (this.state.changesDone == false) {
            return (
                <TouchableOpacity style={Styles.normalButton}>
                    <Text style={Styles.BoldWhiteText}>Ok</Text>
                </TouchableOpacity>
            )
        }
        else if (this.state.changesDone == true) {
            return (
                <TouchableOpacity style={Styles.normalButton}>
                    <Text style={Styles.BoldWhiteText}>Confirm Changes</Text>
                </TouchableOpacity>
            )
        }
    }
    render() {
        return (
            <Modal visible={this.state.visibleGarageServicesModal} transparent={true} onRequestClose={() => { this.props.onExit(false) }}>
                <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', backgroundColor: 'rgba(1,1,1,0.7)' }}>
                    <View style={{ flex: 0.7, backgroundColor: '#fff', borderRadius: 15 }}>
                        <View style={{ flex: 0.1, paddingLeft: responsiveWidth(2), backgroundColor: '#229CE3', borderTopLeftRadius: 15, borderTopRightRadius: 15 }}>
                            <Text style={Styles.intermediateBoldText}>Garage Services</Text>

                        </View>
                        <View style={{ flex: 0.05, alignItems: 'center', marginHorizontal: responsiveWidth(1.5), flexDirection: 'row', }}>
                            <Text style={Styles.BoldBlueText}>Vehicles Type :  </Text>
                            <View style={{ marginLeft: responsiveWidth(2), flexDirection: 'row' }}>
                                <TouchableOpacity style={{ flexDirection: 'row' }}
                                    onPress={() => { this.setState({ radio2: true, radio1: false, vechicleTypes: '2Wheel' }) }}>
                                    <Radio selected={this.state.radio2}
                                        color={"#229CE3"}
                                        selectedColor={'#229CE3'}
                                        onPress={() => { this.setState({ radio2: true, radio1: false, vechicleTypes: '2Wheel' }) }} />
                                    <Text style={{ marginLeft: responsiveWidth(1) }}>2 Wheeler</Text>
                                </TouchableOpacity>

                                <TouchableOpacity style={{ flexDirection: 'row', marginLeft: responsiveWidth(6) }}
                                    onPress={() => { this.setState({ radio1: true, radio2: false, vechicleTypes: '4Wheel' }) }}>
                                    <Radio selected={this.state.radio1}
                                        color={"#229CE3"}
                                        selectedColor={'#229CE3'}
                                        onPress={() => { this.setState({ radio1: true, radio2: false, vechicleTypes: '4Wheel' }) }} />
                                    <Text style={{ marginLeft: responsiveWidth(1) }}>4 Wheeler</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                        <View style={{ flex: 0.75, width: responsiveWidth(90), alignItems: 'center', }}>
                            {this.renderScreen()}
                        </View>
                        <View style={{ flex: 0.1, flexDirection: 'row', height: responsiveHeight(6), width: responsiveWidth(90), alignItems: 'center', justifyContent: 'center', }}>
                            <View style={{ flex: 0.5 }}>
                                <TouchableOpacity style={Styles.normalButton} onPress={() => { this.props.onExit(false) }}>
                                    <Text style={Styles.BoldWhiteText}>Cancel</Text>
                                </TouchableOpacity>
                            </View>
                            <View style={{ flex: 0.5 }}>
                                {this.renderButton()}
                            </View>
                        </View>
                    </View>
                </View>
            </Modal>
        );
    }
}

