import React, { Component } from 'react';
import { View, Text, SafeAreaView, TouchableOpacity, TextInput, Modal, ScrollView } from 'react-native';
import { responsiveScreenHeight, responsiveWidth, responsiveHeight } from 'react-native-responsive-dimensions';
import Styles from '../Styles/Styles';
import { Radio, Icon, Card } from 'native-base';
import { TouchableWithoutFeedback } from 'react-native-gesture-handler';

class DeleteAccountModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            visibleModal: true,
            radio1: false,
            radio2: false,
            radio3: false,
            buttonText: 'Show',
            secureTextEntry: true,
            errorMessage: '',
            pass: '',
            selectedReason: 'none'
        };
    }

    renderButton() {
        if (this.state.buttonText == "Show") {
            return (
                <View style={{ marginLeft: responsiveWidth(3) }}>
                    <TouchableOpacity onPress={() => { this.setState({ buttonText: "Hide", secureTextEntry: false }) }}>
                        <Text style={{ color: '#33f' }}>Show</Text>
                    </TouchableOpacity>
                </View>
            )
        }
        else if (this.state.buttonText == "Hide") {
            return (
                <View style={{ marginLeft: responsiveWidth(3) }}>
                    <TouchableOpacity onPress={() => { this.setState({ buttonText: "Show", secureTextEntry: true }) }}>
                        <Text style={{ color: '#33f' }}>
                            Hide
                        </Text>
                    </TouchableOpacity>
                </View>
            )
        }
    }

    verifyDetails() {

        let { radio1, radio2, radio3, selectedReason, pass } = this.state
        let passw = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,20}$/;
        if (selectedReason == "none" || pass == "") {
            this.setState({ errorMessage: "Please fill the information" })
        }
        else {
            if (pass.match(passw)) {
                alert("Account Deleted Because " + selectedReason)
            }
            else {
                this.setState({ errorMessage: "Wrong Password" })
            }
        }
        setTimeout(() => { this.setState({ errorMessage: '' }) }, 4000)
    }
    render() {
        return (
            <Modal visible={this.state.visibleModal} transparent={true} onRequestClose={() => { this.props.onExit(false) }}>
                <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', backgroundColor: 'rgba(1,1,1,0.7)' }} >
                    <Card style={{ width: responsiveWidth(90), height:responsiveHeight(50), backgroundColor: '#fff', borderRadius: 15 }} >
                        <View style={{ flex: 0.3, paddingLeft: responsiveWidth(2), backgroundColor: '#229CE3', borderTopLeftRadius: 15, borderTopRightRadius: 15 }}>
                            <Text style={Styles.HeaderText}>Delete Account ?</Text>
                            <View style={{ flexDirection: 'row' }}>
                                <View style={{ flex: 0.85, alignItems: 'flex-end', paddingHorizontal: responsiveWidth(2) }}></View>
                                <View style={{ flex: 0.15, alignItems: "flex-end", paddingRight: responsiveWidth(2) }}>
                                    <TouchableOpacity onPress={() => { this.props.onExit(false) }}>
                                        <Text style={Styles.BoldWhiteText}>Exit</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                        <View style={{ width: responsiveWidth(90), flex: 0.7, }}>
                            <ScrollView>
                            <View style={{flex:0.45, paddingLeft: responsiveWidth(2),paddingTop:responsiveWidth(1) }}>
                                <Text style={Styles.boldBlackText}>Tell us why you are you Deleting Acoount?</Text>
                                <TouchableOpacity style={{ flexDirection: 'row' }}
                                    onPress={() => { this.setState({ radio1: true, radio2: false, radio3: false, radio4: false, selectedReason: 'Not Mentioned' }) }}>
                                    <Radio selected={this.state.radio1}
                                        onPress={() => { this.setState({ radio1: true, radio2: false, radio3: false, radio4: false, selectedReason: 'Not Mentioned' }) }} />
                                    <Text style={{ marginLeft: responsiveWidth(1) }}>Rather not mention</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={{ flexDirection: 'row' }}
                                    onPress={() => { this.setState({ radio1: false, radio2: true, radio3: false, radio4: false, selectedReason: 'Less Profit' }) }}>
                                    <Radio selected={this.state.radio2}
                                        onPress={() => { this.setState({ radio1: false, radio2: true, radio3: false, radio4: false, selectedReason: 'Less Profit' }) }} />
                                    <Text style={{ marginLeft: responsiveWidth(1) }}>Not getting enough Profit</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={{ flexDirection: 'row' }}
                                    onPress={() => { this.setState({ radio1: false, radio2: false, radio3: true, radio4: false, selectedReason: 'UI Problems' }) }}>
                                    <Radio selected={this.state.radio3}
                                        onPress={() => { this.setState({ radio1: false, radio2: false, radio3: true, radio4: false, selectedReason: 'UI Problems' }) }} />
                                    <Text style={{ marginLeft: responsiveWidth(1) }}>Didn't liked the UI</Text>
                                </TouchableOpacity>
                            </View>
                            <View style={{ flex:0.2,marginTop: responsiveWidth(4), width: responsiveWidth(90), padding: responsiveWidth(2) }}>
                                <Text style={Styles.boldBlackText}>
                                    Enter Account Password
                            </Text>
                                <View style={{ flexDirection: 'row', marginHorizontal: responsiveWidth(2), alignItems: 'center' }}>
                                    <Icon name="unlock" type="Feather" />
                                    <TextInput style={{ width: responsiveWidth(50), borderBottomWidth: 1, marginLeft: responsiveWidth(0.5) }}
                                        secureTextEntry={this.state.secureTextEntry}
                                        onChangeText={(pass) => { this.setState({ pass: pass }) }} />
                                    {this.renderButton()}
                                </View>

                            </View>
                            <View style={{flex:0.1, width: responsiveWidth(90), alignItems: 'center',}}>
                                <Text style={{ color: '#f11' }}>{this.state.errorMessage}</Text>
                            </View>
                            <View style={{flex:0.25, width: responsiveWidth(90), alignItems: "center",  }}>
                                <TouchableOpacity style={Styles.normalButton}
                                    onPress={() => { this.verifyDetails() }}>
                                    <Text style={Styles.BoldWhiteText}>Delete Account</Text>
                                </TouchableOpacity>
                            </View>
                            </ScrollView>
                        </View>
                    </Card>
                </View>
            </Modal>
        );
    }
}

export default DeleteAccountModal;
