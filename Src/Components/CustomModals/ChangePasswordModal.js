import React, { Component } from 'react';
import { View, Text, SafeAreaView, TextInput, TouchableOpacity,Modal } from 'react-native';
import { Card } from 'native-base';
import { responsiveHeight, responsiveWidth } from 'react-native-responsive-dimensions';
import Styles from '../Styles/Styles';

class ChangePasswordModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modalVisible:true,
            errorMessage: '',
            oldPass: '',
            newPass: '',
            cPass: ''
        };
    }

    verifyDetails() {
        var { oldPass, newPass, cPass } = this.state
        var passformat = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,20}$/;
        if (oldPass == "" || newPass == "" || cPass == "") {
            this.setState({ errorMessage: 'Please fill all details' })
        }
        else {
            if (oldPass.match(passformat)) {
                if (newPass.match(passformat)) {
                    if (cPass == newPass) {
                        this.setState({errorMessage:'Password Changed'})
                        this.props.onConfirm(false)
                    }
                    else {
                        this.setState({ errorMessage: "Password didn't match" })
                    }
                }
                else {
                    this.setState({ errorMessage: 'Password length must be more than 8 and a combination of Capital,Small leters and Numbers' })
                }
            }
            else {
                this.setState({ errorMessage: 'Old Password is wrong' })
            }
        }
        setTimeout(() => { this.setState({ errorMessage: '' }) }, 4000)
    }

    render() {
        return (
            <Modal visible={this.state.modalVisible} transparent={true} onRequestClose={() => this.props.onAction(false)}>
                <SafeAreaView style={{ flex: 1, backgroundColor: 'rgba(1,1,1,0.7)', alignItems: 'center', justifyContent: 'center' }}>
                    <Card style={{ height: responsiveHeight(40), width: responsiveWidth(95), borderRadius: 15 }}>
                        <View style={{ marginLeft: responsiveWidth(2) }}><Text style={Styles.HeaderBlueText}>Change Password</Text></View>
                        <View style={{ width: responsiveWidth(95), alignItems: 'center' }}>
                            <TextInput style={Styles.modalINput}
                                onChangeText={(oldPass) => { this.setState({ oldPass: oldPass }) }}
                                secureTextEntry={true}
                                placeholder="Enter Old Password"
                                placeholderTextColor="#666"
                                keyboardType="default"
                                autoCorrect={false}
                                returnKeyType="next"
                                onSubmitEditing={() => { this.refs.newpass.focus() }}
                            />
                            <TextInput style={Styles.modalINput}
                                onChangeText={(newPass) => { this.setState({ newPass: newPass }) }}
                                secureTextEntry={true}
                                placeholder="Enter New Password"
                                placeholderTextColor="#666"
                                keyboardType="default"
                                autoCorrect={false}
                                returnKeyType="next"
                                ref={'newpass'}
                                onSubmitEditing={() => { this.refs.cpass.focus() }} />

                            <TextInput style={Styles.modalINput}
                                onChangeText={(cPass) => { this.setState({ cPass: cPass }) }}
                                secureTextEntry={true}
                                placeholder="Confirm New Password"
                                placeholderTextColor="#666"
                                keyboardType="default"
                                autoCorrect={false}
                                returnKeyType="go"
                                ref={'cpass'}
                                onSubmitEditing={() => { this.verifyDetails() }} />


                        </View>
                        <View style={{ width: responsiveWidth(90), alignItems: 'center' }}>
                            <Text style={{ color: '#f11' }}>{this.state.errorMessage}</Text>
                        </View>
                        <View style={{ width: responsiveWidth(90), height: responsiveHeight(10), alignItems: 'center', justifyContent: 'center', flexDirection: 'row' }}>
                            <TouchableOpacity style={Styles.normalButton} onPress={() => { this.props.onAction(false) }}>
                                <Text style={Styles.BoldWhiteText}>Cancel</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={Styles.normalButton} onPress={() => { this.verifyDetails() }}>
                                <Text style={Styles.BoldWhiteText}>Submit</Text>
                            </TouchableOpacity>
                        </View>

                    </Card>
                </SafeAreaView>
            </Modal>
        );
    }
}

export default ChangePasswordModal;
