import { StyleSheet } from 'react-native'
import { responsiveHeight, responsiveWidth, responsiveFontSize } from 'react-native-responsive-dimensions'

export default StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: '#229CE3'
    },
    authInput: {
        height: responsiveHeight(5),
        backgroundColor: 'rgba(255,255,255,0.2)',
        color: '#fff',
        paddingHorizontal: responsiveWidth(5),
        borderRadius: 10,
        marginBottom: responsiveWidth(2),
    }
    ,
    modalINput: {
        height: responsiveHeight(5),
        width:responsiveWidth(85),
        borderBottomWidth:1,
        borderColor:'#229CE3',
        color: '#111',
        paddingHorizontal: responsiveWidth(2),
        marginBottom: responsiveWidth(4),
    },
    BoldWhiteText: {
        color: '#fff',
        fontWeight: 'bold',
        fontSize:responsiveFontSize(2.2)
    },
    BoldBlueText: {
        color: '#229CE3',
        fontWeight: 'bold',
        fontSize:responsiveFontSize(2.2)
    },
    boldBlackText: {
        color: '#111',
        fontWeight: 'bold',
    },
    HeaderText:{
        color:'#fff',
        fontWeight:'bold',
        fontSize:responsiveFontSize(4),
        width:responsiveWidth(60)
    },
    HeaderBlueText:{
        color:'#229CE3',
        fontSize:responsiveFontSize(4),
        fontWeight:'bold',
       
    },
    intermediateBoldText:{
        color:'#fff',
        fontSize:responsiveFontSize(2.5),
        fontWeight:'bold',
        width:responsiveWidth(70)
    },
    WhitesubmitButton: {
        paddingVertical: responsiveWidth(3),
        borderWidth: 2,
        borderColor: '#fff',
        borderRadius: 10,
        alignItems: 'center', justifyContent: 'center',
        width: responsiveWidth(40),
        marginHorizontal: responsiveWidth(3)
    },
    BluesubmitButton: {
        paddingVertical: responsiveWidth(3),
        backgroundColor: '#fff',
        borderRadius: 10,
        alignItems: 'center', justifyContent: 'center',
        width: responsiveWidth(40),
        marginHorizontal: responsiveWidth(3),
        height:responsiveHeight(7)
    },
    normalButton:{
        backgroundColor:'#229CE3',
        borderRadius:10,
        width:responsiveWidth(40),
        paddingVertical:responsiveWidth(2.5),
        alignItems:'center',justifyContent:'center',
        marginHorizontal:responsiveWidth(3)
    },
    BlueBorderButton: {
        paddingVertical: responsiveWidth(2),
        borderWidth: 2,
        borderColor: '#229CE3',
        borderRadius: 10,
        alignItems: 'center', justifyContent: 'center',
        width: responsiveWidth(40),
        marginHorizontal: responsiveWidth(3),
        height:responsiveHeight(6)
    },
    DashboardMainCard: {
        backgroundColor: '#229CE3',
        width: responsiveWidth(40),
        height: responsiveHeight(20),
        borderRadius: 20,
        marginHorizontal: responsiveWidth(4),
        alignItems: 'center',
        justifyContent: 'center'
    },
    DashboardMainIcon: {
        width: responsiveWidth(20), 
        height: responsiveHeight(10.3), 
        marginBottom: responsiveWidth(3)
    },
    GarageSetupLogo:{
        height:responsiveHeight(8),
        width:responsiveWidth(33)
    },
    GarageSetupCustomCard:{
        height:responsiveHeight(80),
        width:responsiveWidth(90),
        borderRadius:10
    },
    GarageSetupCustomInputs:{
        width:responsiveWidth(80),
        height:responsiveHeight(3),
        borderBottomWidth:1,
        borderBottomColor:'#229CE3',
        
    }


})