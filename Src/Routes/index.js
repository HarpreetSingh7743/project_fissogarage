import {createStackNavigator} from "react-navigation-stack";
import {createAppContainer,createSwitchNavigator} from "react-navigation";
import Auth from "./Auth/AuthAppNav";
import Main from "./MainTabs/MainTabNav";
import MainGarage from "./MainTabs/GarageSetupNav";
import Splash from "../Screens/SplashScreen";
import Walkthrough from "../Screens/Walkthrough";

const appContainer = createSwitchNavigator({
    auth:Auth,
    main:Main,
    mainGarage:MainGarage,
    splash:Splash,
    walk:Walkthrough,
   
},{
    initialRouteName:'splash',
    headerMode:null
});

export default createAppContainer(appContainer);