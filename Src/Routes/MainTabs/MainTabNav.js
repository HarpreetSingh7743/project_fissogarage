import React from 'react';
import { createAppContainer } from 'react-navigation';
import { createBottomTabNavigator } from 'react-navigation-tabs';

import Profile from '../../Screens/Main/Profile'
import Requests from '../../Screens/Main/Requests'
import  MainAppNav from '../Main/MainAppNav'
import { Icon,Text,View } from 'native-base';
import {responsiveFontSize,responsiveHeight,responsiveWidth} from "react-native-responsive-dimensions";

const MainTabNav = createBottomTabNavigator({
    // MainAppNav:{
    //     screen:MainAppNav,
    //     navigationOptions:{
    //         title:"Dashboard",
    //         tabBarIcon:()=>{
    //             return(<Icon name="home" type="FontAwesome" style={{color:'#229CE3'}}/>)
    //         }
    //     }
    // },
    MainAppNav:{screen:MainAppNav,
        navigationOptions:{
            tabBarLabel: ({ tintColor, focused }) => (
              <View style={{alignItems:"center",justifyContent:"center"}}>
                <Icon name="ios-home" 
                  style={{ fontSize:responsiveFontSize(3.3),
                        marginBottom:responsiveHeight(1),
                        color: focused ? "#229CE3" : "#808080",}}/>
                <Text style={{ fontSize: responsiveFontSize(1.4),
                              color: focused ? "#229CE3" : "#808080"}}>Dashboard</Text>
              </View>
            )
          }
      },
    Requests:{
        screen:Requests,
        navigationOptions:{
            // title:"Requests",
            // tabBarIcon:()=>{
            //     return(<Icon name="message" type="Entypo" style={{color:'#229CE3'}}/>)
            // }
            tabBarLabel: ({ tintColor, focused }) => (
                <View style={{alignItems:"center",justifyContent:"center"}}>
                  <Icon name="message" type="Entypo" style={{color:'#229CE3'}}
                    style={{ fontSize:responsiveFontSize(3.3),
                          marginBottom:responsiveHeight(1),
                          color: focused ? "#229CE3" : "#808080",}}/>
                  <Text style={{ fontSize: responsiveFontSize(1.4),
                                color: focused ? "#229CE3" : "#808080"}}>Requests</Text>
                </View>
              )
        }
    },
    Profile:{
        screen:Profile,
        navigationOptions:{
            // title:"Profile",
            // tabBarIcon:()=>{
            //     return(<Icon name="user" type="Entypo" style={{color:'#229CE3'}}/>)
            // }
            tabBarLabel: ({ tintColor, focused }) => (
                <View style={{alignItems:"center",justifyContent:"center"}}>
                  <Icon name="user" type="Entypo" style={{color:'#229CE3'}}
                    style={{ fontSize:responsiveFontSize(3.3),
                          marginBottom:responsiveHeight(1),
                          color: focused ? "#229CE3" : "#808080",}}/>
                  <Text style={{ fontSize: responsiveFontSize(1.4),
                                color: focused ? "#229CE3" : "#808080"}}>Profile</Text>
                </View>
              )
        }
    },
  },{
      initialRouteName:'MainAppNav',
      headerMode:'null',
      tabBarOptions:{
          activeTintColor:'#229CE3',
          inactiveTintColor:'#111'
      }
  });
  
  export default createAppContainer(MainTabNav);