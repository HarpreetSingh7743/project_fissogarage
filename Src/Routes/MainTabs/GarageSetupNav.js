import { createAppContainer } from "react-navigation";
import { createStackNavigator } from "react-navigation-stack";
import UserInfo from '../../Screens/Main/SetupGarage/UserInfo'
import GarageInfo from '../../Screens/Main/SetupGarage/GarageInfo'
import GarageVehicles from '../../Screens/Main/SetupGarage/GarageVehicles'
import GarageServices from '../../Screens/Main/SetupGarage/GarageServices'

const GarageSetupNav=createStackNavigator({
    UserInfo:UserInfo,
    GarageInfo:GarageInfo,
    GarageVehicles:GarageVehicles,
    GarageServices:GarageServices
},{
    initialRouteName:"UserInfo",
    headerMode:'null'
    
})

export default createAppContainer(GarageSetupNav)