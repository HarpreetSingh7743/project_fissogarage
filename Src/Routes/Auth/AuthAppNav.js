import React from 'react';
import { View, Text } from 'react-native';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';

import SignIn from '../../Screens/Auth/SignIn';
import SignUp from '../../Screens/Auth/SignUp';
import Forgot from '../../Screens/Auth/Forgot';
import EnterOTPModal from "../../Components/CustomModals/EnterOTPModal";

const AuthAppNav = createStackNavigator({
    SignIn:SignIn,
    SignUp:SignUp,
    Forgot:Forgot,
    enterOtpmodal:EnterOTPModal
  },{
      initialRouteName:'SignIn',
      headerMode:'null'
  });
  
  export default createAppContainer(AuthAppNav);