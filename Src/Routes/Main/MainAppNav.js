
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import Dashboard from '../../Screens/Main/Dashboard'
import History from '../../Screens/Main/History'
import AllAppointments from '../../Screens/Main/AllAppointments'
import TodayAppointments from '../../Screens/Main/TodayAppointments'
import JobStatus from '../../Screens/Main/JobStatus'

const MainAppNav = createStackNavigator({
    Dashboard:Dashboard,
    AllAppointments:AllAppointments,
    TodayAppointments:TodayAppointments,
    JobStatus:JobStatus,
    History:History
  },{
      initialRouteName:'Dashboard',
      headerMode:'null'
  });
  
  export default createAppContainer(MainAppNav);