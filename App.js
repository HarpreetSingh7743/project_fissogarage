// import React, { Component } from 'react';
// import { Root } from 'native-base';

// //import MainTabNav from './Src/Routes/MainTabs/MainTabNav';

// import { SafeAreaProvider } from 'react-native-safe-area-context';

// export default class App extends Component {
//   render() {
//     return (
//       <SafeAreaProvider>
//         <Root>
//           {/* <MainTabNav /> */}
//         </Root>
//       </SafeAreaProvider>
//     );
//   }
// }
import React from 'react';
import {Root} from "native-base";
import { SafeAreaProvider,SafeAreaView } from 'react-native-safe-area-context';
import AppContainer from "./Src/Routes/index";
import { StatusBar } from 'react-native';

class App extends React.Component {
  render(){
    return (
      <Root>
        <SafeAreaProvider>
          {/* <SafeAreaView style={{flex:1}}> */}
            <StatusBar barStyle="light-content"/>
            <AppContainer />
          {/* </SafeAreaView> */}
        </SafeAreaProvider>
      </Root>
    );
  }
}
export default App;

