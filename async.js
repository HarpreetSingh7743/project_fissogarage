import {AsyncStorage} from "react-native";

const LocalStorage = {
    retrieveItem : async (key) => {
        return new Promise(async (resolve, reject) => {
            try {
                const retrievedItem = await AsyncStorage.getItem(key);
                const item = JSON.parse(retrievedItem);
                resolve(item);
            } catch (error) {
                reject(error);
            }
        })
      },
    storeItem : async (key,Item) => {
        return new Promise(async (resolve, reject) => {
            try {
                var storedItem = await AsyncStorage.setItem(key, JSON.stringify(Item));
                const item = JSON.parse(storedItem);
                resolve(item);
            } catch (error) {
                reject(error);
            }
        })
    },
    removeItem : async (key) => {
        return new Promise(async (resolve, reject) => {
            try {
                var removedItem = await AsyncStorage.removeItem(key);
                const item = JSON.parse(removedItem);
                resolve(item);
            } catch (error) {
                reject(error);
            }
        })
    }    
    
      

}

export default LocalStorage;